<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Karyawan_m');
        $this->load->model('Master_m');
        $this->load->model('Analisa_m');

        // if(!$this->Login_m->logged_id())
        // {
        //     session_destroy();
        //     redirect('login');         
        // }
    }

    function index()
    {
        $data['title']      = 'Beranda';
        $data['sub_menu']   = 0;
        $data['page_id']    = 1;

        $kantor = $this->session->userdata('id_kantor');

        if ($this->session->userdata('id_jabatan')  == 1) { //PIC
            $data['dokumen']        = $this->Analisa_m->dokumen_pic($kantor);
            $data['akad']           = $this->Analisa_m->akad_pic($kantor);
            $data['akad_pending']   = $this->Analisa_m->akad_pending_pic($kantor);
            $data['tutup']          = $this->Analisa_m->tutup_pic($kantor);
        } else if ($this->session->userdata('id_jabatan') == 2 or 
                  $this->session->userdata('id_jabatan')  == 6 or 
                  $this->session->userdata('id_jabatan')  == 7 or 
                  $this->session->userdata('id_jabatan')  == 8 ){ 
            $data['dokumen']        = $this->Analisa_m->dokumen_ks($kantor);
            $data['akad']           = $this->Analisa_m->akad_ks($kantor);
            $data['akad_pending']   = $this->Analisa_m->akad_pending_ks($kantor);
            $data['tutup']          = $this->Analisa_m->tutup_ks($kantor);
        } else if ($this->session->userdata('id_jabatan')  == 3 or 
                   $this->session->userdata('id_jabatan')  == 4 or 
                   $this->session->userdata('id_jabatan')  == 5 or 
                   $this->session->userdata('id_jabatan')  == 9 ) { 
            $data['dokumen']        = $this->Analisa_m->dokumen_resa($kantor);
            $data['akad']           = $this->Analisa_m->akad_resa($kantor);
            $data['akad_pending']   = $this->Analisa_m->akad_pending_resa($kantor);
            $data['tutup']          = $this->Analisa_m->tutup_resa($kantor);
        } else if ($this->session->userdata('id_jabatan')  == 99) { //ADMINISTRATOR
            $data['dokumen']        = $this->Analisa_m->dokumen_resa($kantor);
            $data['akad']           = $this->Analisa_m->akad_resa($kantor);
            $data['akad_pending']   = $this->Analisa_m->akad_pending_resa($kantor);
            $data['tutup']          = $this->Analisa_m->tutup_resa($kantor);
        } else if ($this->session->userdata('id_jabatan')  == 13) { 
            $data['dokumen']        =  '';
            $data['akad']           =  '';
            $data['akad_pending']   =  '';
            $data['tutup']          =  '';
        }


        if ($this->session->userdata('id_jabatan') == 13) {
            $this->template->load('template','content_bank',$data);
        } else {
            $this->template->load('template','content',$data);
        }
    }

    function update_password()
    {
        extract($_POST);
        $data = array(
                'password'      => md5($password)
        );

        $this->db->where('id_user',$id_user);
        $this->db->update('tm_user',$data);
        redirect('login/logout');
    }


}