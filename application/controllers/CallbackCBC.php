<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CallBackRiau extends CI_Controller {

    private $secretkey = 'Password;;';

    function __construct()
    {
        parent::__construct();

    }

    function index()
    {
        // return $this->session->userdata('KodeUser');
    }

    function getToken()
    {
      
        $jsondata = array(
            'client_id' => 'clientidBRK',
            'client_secret' => 'clientsecretBRK',
            'username' => 'usernameBRK',
            'password' => 'passwordBRK',
            'grand_type' => 'password credential',
            'scope' => 'roles'
        );

        $ch = curl_init();
        $data_string = json_encode($jsondata);
        // curl_setopt($ch, CURLOPT_HEADER, false);
        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi"); // Isi dengan URL dari Bank Riau masih menunggu
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        // curl_setopt($ch, CURLOPT_PORT, 888);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        // if(curl_errno($ch)){
        //     $err = curl_error($ch);
        //     echo $err;
        // }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            echo $httpcode;
            if($httpcode == '200'){
                $json = json_decode($result, true);

            if($json['Result']['kode_response'] == '00'){
                echo $json['Result']['token'];
            }else{
                echo "Error";
            }
        }
        
     }

    function UpdateDataCBC(){
        $data = $this->input->raw_input_stream;
        // var_dump($data);
        // die();
        $data_json = json_decode($data, true); 

        // var_dump($data_json);
        // die();

        //selanjutnya lakukan proses data sesuai sesuai kebutuhan
        $success = true;
        if(!empty($data_json)){

                $sqlupdate = "
                    UPDATE PAN_BRK.dbo.DataRiau
                    SET status_dokumen = '".$data_json['status_dokcbc']."'
                    WHERE id_transaksi = '".$data_json['id_transaksi']."' AND id_transaksi_bank = '".$data_json['id_transaksi_bank']."'
                ";

                $update = $this->db->query($sqlupdate);
                if($update === false) $success = false;
        }

        if($success){
            $return = $this->sendDataCBC($data_json['id_transaksi'], $data_json['id_transaksi_bank']);
            echo $return;
            die();
        }else{
            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Update Data ke Tampungan"));
            echo json_encode($return);
            die();
        }
    }

    function sendDataCBC($id_transaksi, $id_transaksi_bank)
    {  
        $token = $this->getToken();
        
        if($token == 'Error'){
            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Get Token ke Bank"));
            echo json_encode($return);
            die();
        }

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where id_transaksi = '$id_transaksi' and id_transaksi_bank = '$id_transaksi_bank'")->result_array();
            if (!empty($sql)) {
                $asuransi = '';

                foreach ($sql as $key => $value) {
               
                $arr_insert = array(
                    'id_transaksi' => $id_transaksi,
                    'id_transaksi_bank' => $id_transaksi_bank,
                    'id_pengajuan' => $value['id_pengajuan'],
                    'kode_cabang' => $value['kode_cabang'],
                    'kode_broker' => $value['kode_broker'],
                    'nama' => $value['nama'],
                    'ktp' => $value['ktp'],
                    'status_dokcbc' => $value['status_dokcbc'],
                    'premi_disetujui' => $value['premi_disetujui'],
                    'keterangan' => $value['keterangan']
                    );

                $ch = curl_init();
                $data_string = json_encode($arr_insert);
                // curl_setopt($ch, CURLOPT_HEADER, false);
                // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi"); // Isi dengan URL dari Bank Riau masih menunggu
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                // curl_setopt($ch, CURLOPT_PORT, 888);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                            'Content-Type: application/json',
                                                            'Authorization:Bearer '.$token
                                                        ));
                // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);

                // if(curl_errno($ch)){
                //     $err = curl_error($ch);
                //     echo $err;
                // }
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    // echo $httpcode;
                        if($httpcode == '200'){
                            $json = json_decode($result, true);

                        if($json['Result']['kode_response'] == '00'){
                            $return = array("Result"=>array('status' => '200', 'kode_response' => '00', "message"=>"Berhasil Update Data ke Bank"));
                            echo json_encode($return);
                        }else{
                            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Update Data ke Bank"));
                            echo json_encode($return);
                        }
                    }
                }
            
            }
    }

    function sendDataDebitur($id_transaksi, $norek)
    {  
        $token = $this->getToken();
        
        if($token == 'Error'){
            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Get Token ke Bank"));
            echo json_encode($return);
            die();
        }

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where id_transaksi = '$id_transaksi' and nomor_rekening = '$norek'")->result_array();
            if (!empty($sql)) {
                $asuransi = '';

                foreach ($sql as $key => $value) {
               
                // $arr_insert = array(
                //     'id_transaksi' => $id_transaksi,
                //     'kode_cabang' => $value['kode_cabang'],
                //     'kode_broker' => $value['kode_broker'],
                //     'nama' => $value['nama'],
                //     'ktp' => $value['ktp'],
                //     'nomor_rekening' => $value['nomor_rekening'],
                //     'no_akad' => $value['no_akad'],
                //     'status_callback' => '-',
                //     'resitusi' => array(
                //             'Id_transaksi_bank' => $value['id_transaksi_bank'],
                //             'id_pengajuan' => $value['id_pengajuan'],
                //             'status_restitusi' => $value['status_restitusi'],
                //             'tenor' => $value['tenor'],
                //             'premi' => $value['premi'],
                //             'periode_awal' => $value['periode_awal'],
                //             'periode_akhir' => $value['periode_akhir'],
                //             'tenor_berjalan' => $value['tenor_berjalan'],
                //             'sisa_tenor' => $value['sisa_tenor'],
                //             'status_bayar' => $value['status_bayar'],
                //             'premi_dikembalikan' => $value['premi_dikembalikan'],
                //             'asuransi' => $value['asuransi'],
                //             'keterangan' => '',
                //     ),
                //     'klaim' => array(
                //             'id_transaksi_bank' => $value['id_transaksi_bank'],
                //             'id_pengajuan' => $value['id_pengajuan'],
                //             'status_klaim' => $value['status_klaim'],
                //             'status_bayar' => $value['status_bayar'],
                //             'klaim_dibayarkan' => $value['klaim_dibayarkan'],
                //             'asuransi' => $value['asuransi'],
                //             'keterangan' => '-',
                //     )
                //     );


                // =========> Try Dummy Data

                $arr_insert = array(
                    'id_transaksi' => '009',
                    'kode_cabang' => '103',
                    'kode_broker' => '003',
                    'nama' => 'Hana Setiawan',
                    'ktp' =>'147100812510640082',
                    'nomor_rekening' => '147100812510640082',
                    'no_akad' => '110.3.10.2018.102',
                    'status_callback' => '1',
                    'resitusi' => array(
                            'Id_transaksi_bank' =>'',
                            'id_pengajuan' => '',
                            'status_restitusi' => '',
                            'tenor' => '',
                            'premi' => '',
                            'periode_awal' => '',
                            'periode_akhir' => '',
                            'tenor_berjalan' =>'',
                            'sisa_tenor' => '',
                            'status_bayar' => '',
                            'premi_dikembalikan' => '',
                            'asuransi' => '',
                            'keterangan' => '',
                    ),
                    'klaim' => array(
                            'id_transaksi_bank' => '112',
                            'id_pengajuan' => 'PCLAIM-001',
                            'status_klaim' => '6',
                            'status_bayar' => '1',
                            'klaim_dibayarkan' => '185000000',
                            'asuransi' => 'Askrindo',
                            'keterangan' => '-',
                    )
                    );

                $ch = curl_init();
                $data_string = json_encode($arr_insert);
                // curl_setopt($ch, CURLOPT_HEADER, false);
                // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi"); // Isi dengan URL dari Bank Riau masih menunggu
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                // curl_setopt($ch, CURLOPT_PORT, 888);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                            'Content-Type: application/json',
                                                            'Authorization:Bearer '.$token
                                                        ));
                // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);

                // if(curl_errno($ch)){
                //     $err = curl_error($ch);
                //     echo $err;
                // }
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    // echo $httpcode;
                        if($httpcode == '200'){
                            $json = json_decode($result, true);

                        if($json['Result']['kode_response'] == '00'){
                            $return = array("Result"=>array('status' => '200', 'kode_response' => '00', "message"=>"Berhasil Update Data ke Bank"));
                            echo json_encode($return);
                        }else{
                            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Update Data ke Bank"));
                            echo json_encode($return);
                        }
                    }
                }
            
            }
    }
}