<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumenriau extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Dokumenriau_m');

        // if(!$this->Login_m->logged_id())
        // {
        //     session_destroy();
        //     redirect('login');         
        // }
    }

    function index()
    {
        $data['title']      = 'Dokumen Riau';
        $data['sub_menu']   = 0;
        $data['page_id']    = 1;

        $data['datana']         = $this->Dokumenriau_m->dokumen();

            $this->template->load('template','dokumenriau',$data);
    }

}