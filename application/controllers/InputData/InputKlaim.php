<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class InputKlaim extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        $this->getdatariau();
    }

    public function getdatariau(){

        $data = $this->input->raw_input_stream;

        $data_json = json_decode($data, true);

        $success = true;

        $jwt = $this->input->get_request_header('Authorization');

        $token = null;
 
        if(!empty($jwt)) {
            if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                $token = $matches[1];
            }
        }

        if(is_null($token) || empty($token)) {
            $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

        $cek_token = $this->db->query("select * from Tampung_Riau.dbo.Get_Token where CONVERT(VARCHAR(MAX), token) = '$token' ")->result_array();

        date_default_timezone_set('Asia/Jakarta');

        if ( strtotime(date("Y-m-d H:i:s")) > strtotime($cek_token[0]['date_expired']) ) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

        $pk = $data_json['no_akad'];
        $cekpk = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where pk = '$pk' ")->result_array();
        $plafon = $cekpk[0]['amount'];
        $ajuklaim = $data_json['jumlah_diajukan'];
        if(empty($cekpk)){
            $return = array("Result"=>array('status' => '200', 'kode_response' => '121', "message"=>"Nomor akad tidak ditemukan"));
            echo json_encode($return);
            die;
        }

        if((int)$ajuklaim > (int)$plafon){
            $return = array("Result"=>array('status' => '200', 'kode_response' => '122', "message"=>"Klaim yang diajukan melebihi uang pertanggungan"));
            echo json_encode($return);
            die;
        }

        if ( strtotime(date("Ymd")) > strtotime($data_json['tanggal_kejadian']) ) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '123', "message"=>"Tanggal kejadian klaim terjadi Setelah Hari Ini"));
        echo json_encode($return);
        die;
        }

        if ( strtotime($data_json['tanggal_kejadian']) < strtotime($data_json['periode_awal'])) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '124', "message"=>"Tanggal kejadian klaim terjadi sebelum waktu akad dimulai"));
        echo json_encode($return);
        die;
        }

        if ( strtotime($data_json['tanggal_kejadian']) > strtotime($data_json['periode_akhir'])) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '124', "message"=>"Tanggal kejadian klaim terjadi setelah waktu akad berakhir"));
        echo json_encode($return);
        die;
        }

            if ($data_json) {
                date_default_timezone_set('Asia/Jakarta');
                $arr_insert = array(
                        'id_transaksi' => $data_json['id_transaksi'],
                        'kode_broker' => $data_json['kode_broker'],
                        'ktp' => $data_json['ktp'],
                        'nama' => $data_json['nama'],
                        'kode_cabang' => $data_json['kode_cabang'],
                        'nomor_rekening' => $data_json['nomor_rekening'],
                        'no_pk' => $data_json['no_akad'],
                        'tenor' => $data_json['tenor'],
                        'premi' => $data_json['premi'],
                        'periode_awal' => $data_json['periode_awal'],
                        'periode_akhir' => $data_json['periode_akhir'],
                        'tenor_berjalan' => $data_json['tenor_berjalan'],
                        'sisa_tenor' => $data_json['sisa_tenor'],
                        'jenis_klaim' => $data_json['jenis_klaim'],
                        'tanggal_kirim' => $data_json['tanggal_kirim'],
                        'penyebab_klaim' => $data_json['penyebab_klaim'],
                        'tanggal_kejadian' => $data_json['tanggal_kejadian'],
                        'tempat_kejadian' => $data_json['tempat_kejadian'],
                        'jumlah_diajukan' => $data_json['jumlah_diajukan'],
                        'tujuan_pembayaran' => $data_json['tujuan_pembayaran'],
                        
                    );

                $insert = $this->db->query("insert into Tampung_Riau.dbo.Pengajuan_Klaim (
                 id_transaksi,
                 kode_broker,
                 kode_cabang,
                 no_pk,
                 nomor_rekening,
                 nama,
                 tenor,
                 ktp,
                 periode_awal,
                 periode_akhir,
                 tenor_berjalan,
                 sisa_tenor,
                 jenis_klaim,
                 tanggal_kirim,
                 penyebab_klaim,
                 tanggal_kejadian,
                 tempat_kejadian,
                 jumlah_diajukan,
                 tujuan_pembayaran,
                 premi
                 ) 
                 values (
                 '".$arr_insert['id_transaksi']."',
                 '".$arr_insert['kode_broker']."',
                 '".$arr_insert['kode_cabang']."',
                 '".$arr_insert['no_pk']."',
                 '".$arr_insert['nomor_rekening']."',
                 '".$arr_insert['nama']."',
                 '".$arr_insert['tenor']."',
                 '".$arr_insert['ktp']."',
                 '".$arr_insert['periode_awal']."',
                 '".$arr_insert['periode_akhir']."',
                 '".$arr_insert['tenor_berjalan']."',
                 '".$arr_insert['sisa_tenor']."',
                 '".$arr_insert['jenis_klaim']."',
                 '".$arr_insert['tanggal_kirim']."',
                 '".$arr_insert['penyebab_klaim']."',
                 '".$arr_insert['tanggal_kejadian']."',
                 '".$arr_insert['tempat_kejadian']."',
                 '".$arr_insert['jumlah_diajukan']."',
                 '".$arr_insert['tujuan_pembayaran']."',
                 '".$arr_insert['premi']."'
                 )");

                 if($insert === false){ 
                    $success = false;
                 }
            }else{
            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Data (field) tidak sesuai"));
            echo json_encode($return);
            die;
            }

            if($success === true){

            // $ch = curl_init();
            // $data_string = json_encode($arr_insert);
            // // curl_setopt($ch, CURLOPT_HEADER, false);
            // // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            // // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
            // curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
            // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            // // curl_setopt($ch, CURLOPT_PORT, 888);
            // // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
            // // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            // // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
            // curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            // // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            // // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // curl_exec($ch);
            // curl_close($ch);


            $return = array("Result"=>array('status' => '200', 'kode_response' => '00', "message"=>"Berhasil kirim data pengajuan klaim"));

            echo json_encode($return);
            die;

            } else {
            $return = array("Result"=>array('status' => '400', 'kode_response' => '99', "message"=>"Error lainnya"));
            echo json_encode($return);
            die;
        }
        
    }

    public function kirimdata(){

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where stcurl = 0 ")->result_array();

        // var_dump($sql);
        // die();

                if (!empty($sql)) {
                        $asuransi = '';

                        foreach ($sql as $key => $value) {

                        $norek = $value['norek'];
                        $arr_insert = array(
                        'id_transaksi' => $value['id_transaksi'],
                        'kode_broker' => $value['kode_broker'],
                        'cab' => $value['cab'],
                        'pk' => $value['pk'],
                        'norek' => $value['norek'],
                        'nama' => $value['nama'],
                        'lahir' => $value['lahir'],
                        'buka' => $value['buka'],
                        'tempo' => $value['tempo'],
                        'plan' => $value['plankredit'],
                        'amount' => $value['amount'],
                        'id' => $value['id'],
                        'ktp' => $value['ktp'],
                        'rate' => $value['rate'],
                        'sex' => $value['sex'],
                        'rate_asuransi' => $value['rate'],
                        'asuransi' => $asuransi,
                        'npwp' => $value['npwp'],
                        'old_pk' => $value['old_pk'],
                        'benefit' => $value['benefit'],

                        );
                    }

                        $ch = curl_init();
                        $data_string = json_encode($arr_insert);
                        // curl_setopt($ch, CURLOPT_HEADER, false);
                        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        // curl_setopt($ch, CURLOPT_PORT, 888);
                        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                        // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_exec($ch);
                        curl_close($ch);

                        // if(curl_errno($ch)){
                        //     $err = curl_error($ch);
                        //     echo $err;
                        // }

                        $this->db->query("update Tampung_Riau.dbo.DataRiau set stcurl = '1' where norek = '$norek'");
                    
                }
    }

}
