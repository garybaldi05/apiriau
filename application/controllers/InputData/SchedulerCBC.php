<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class SchedulerCBC extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // ini_set('max_execution_time', 0);
        // set_time_limit(0);
        

    }

    public function index()
    {

        $this->kirimdata();
    }

    public function kirimdata(){

        $this->load->library('ftp');
        

        $sql = $this->db->query("select * from Tampung_Riau.dbo.Scheduler_CBC where stcurl = 0 ")->result_array();

        if (!empty($sql)) {
            try{
                foreach ($sql as $key => $value) {
                    $config['hostname'] = 'ftp.example.com'; // isi dengan hostnama bank riau
                    $config['username'] = 'your-username'; // isi dengan username bank riau
                    $config['password'] = 'your-password'; // isi dengan password bank riau
                    $config['debug'] = TRUE;

                    $this->ftp->connect($config);

                    $name = $value['ktp'].'_001';

                    // $this->ftp->upload('/local/path/to/'.$name.'.zip', '/public_html/path/to/'.$name.'.zip', 'auto', 0775);
                    $downloadRiau = $this->ftp->download('/public_html/path/to/'.$name.'.zip','/local/upload/CBC/'.$name.'.zip', 'auto');
                    if($downloadRiau){
                        $this->ftp->close();

                        $config2['hostname'] = 'ftp.example.com'; // isi dengan hostnama 100.2
                        $config2['username'] = 'your-username'; // isi dengan username 100.2
                        $config2['password'] = 'your-password'; // isi dengan password 100.2
                        $config2['debug'] = TRUE;

                        $this->ftp->connect($config2);

                        $this->ftp->mkdir('/public_html/riaupan/upload/CBC/'.$name, 0755);
                        $upload = $this->ftp->upload('/local/upload/CBC/'.$name.'.zip', '/public_html/riaupan/upload/CBC/'.$name.'/'.$name.'.zip', 'auto', 0775);
                    
                        if($upload){
                            $this->kirimdataApi($name, $value['id_data']);
                        }
                    }
                 }
                 
            }catch( Exception $e ){

            }
            
            
            
        }
    }

    public function kirimdataApi($nama,$id_data){

                        $arr_insert = array(
                        'nama' => $nama
                        );

                        $ch = curl_init();
                        $data_string = json_encode($arr_insert);
                        // curl_setopt($ch, CURLOPT_HEADER, false);
                        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupandev/GetCBC/SaveFile");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        // curl_setopt($ch, CURLOPT_PORT, 888);
                        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                        // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        // if(curl_errno($ch)){
                        //     $err = curl_error($ch);
                        //     echo $err;
                        // }
                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            echo $httpcode;
                            if($httpcode == '200'){
                                $json = json_decode($result, true);

                                if($json['status'] == '200'){
                                        $this->db->query("update Tampung_Riau.dbo.Scheduler_CBC set stcurl = '1' where id_data = '$id_data'");
                                }
                             }
                            //  else{
        
                            // }
                         }
                    
                }
    }

}
