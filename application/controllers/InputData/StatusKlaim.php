<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class StatusKlaim extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $this->getklaim();
    }

    public function getklaim(){

$clientid = 'clientidBRK';
$clientsecret = 'clientsecretBRK';
$username = 'usernameBRK';
$password = 'passwordBRK';

$arr_insert = array(
                'client_id' => $clientid,
                'client_secret' => $clientsecret,
                'username' => $username,
                'password' => $password,
                'grand_type' => 'password credential',
                'scope' => 'roles',
            );

$ch = curl_init();
$data_string = json_encode($arr_insert);
// curl_setopt($ch, CURLOPT_HEADER, false);
// curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
// curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/api/InputData/Get_Token");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
// curl_setopt($ch, CURLOPT_PORT, 888);
// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
// curl_setopt($ch, CURLOPT_TIMEOUT, 120);
// curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$datas = curl_exec($ch);
curl_close($ch);

$data_obj = json_decode($datas);

if ($data_obj->Result->{"kode_response"} == 00) {
    
    $data = $this->input->raw_input_stream;
    $data_json = json_decode($data, true);

    $access_token = $data_obj->Result->{"token"};
    $headers = array(
    "Content-Type: application/json",
    "Authorization: Bearer $access_token"
    );

    $post_params = array(
    'id_transaksi' => $data_json['id_klaim'],
    'nama' => $data_json['nama'],
    'kode_cabang' => $data_json['kode_cabang'],
    'nomor_rekening' => $data_json['nomor_rekening'],
    'no_pk' => $data_json['no_pk'],
    'id_transaksi_pengajuan' => $data_json['id_transaksi'],
    'status_pengajuan' => $data_json['status_pengajuan'],
    'status_bayar' => $data_json['status_bayar'],
    'klaim_dibayarkan' => $data_json['klaim_dibayarkan'],
    'asuransi' => $data_json['asuransi'],
    'keterangan' => $data_json['keterangan'],
    );

    $statuspengajuan = $data_json['status_pengajuan'];
    $statusbayar = $data_json['status_bayar'];
    $klaimbayar = $data_json['klaim_dibayarkan'];
    $keterangan = $data_json['keterangan'];
    $id = $data_json['id_transaksi'];

    $this->db->query("update PAN_BRK.dbo.Pengajuan_Klaim set status_pengajuan = '$statuspengajuan', status_bayar = '$statusbayar', klaim_dibayarkan = '$klaimbayar', keterangan = '$keterangan', status_date = GETDATE() where id_transaksi = '$id'");

    echo json_encode($return);
    die;

    var_dump($data_json);
    die();
}
else{
    $this->getklaim();
}

}
}
