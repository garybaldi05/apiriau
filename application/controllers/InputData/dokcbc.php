<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class dokcbc extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // ini_set('max_execution_time', 0);
        // set_time_limit(0);

    }

    public function index()
    {

        $this->getdatariau();
    }

    public function getdatariau(){

        $data = $this->input->raw_input_stream;

        $data_json = json_decode($data, true);

        $success = true;

        $jwt = $this->input->get_request_header('Authorization');

        $token = null;
 
        if(!empty($jwt)) {
            if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                $token = $matches[1];
            }
        }

        if(is_null($token) || empty($token)) {
            $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

        $cek_token = $this->db->query("select * from Tampung_Riau.dbo.Get_Token where CONVERT(VARCHAR(MAX), token) = '$token' ")->result_array();

        date_default_timezone_set('Asia/Jakarta');

        if ( strtotime(date("Y-m-d H:i:s")) > strtotime($cek_token[0]['date_expired']) ) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

            if ($data_json) {
                date_default_timezone_set('Asia/Jakarta');
                $stcurl = '0';

                $amount = $data_json['plafond'];
                $ab = $data_json['tgl_lahir'];
                $id = $data_json['pekerjaan'];
                $benefit = $data_json['benefit'];
                $tanggal = new DateTime($ab);
                $today = new DateTime('today');
                $y = $today->diff($tanggal)->y;
                $m = $today->diff($tanggal)->m;
                $d = $today->diff($tanggal)->d;
                if ($m >= 6 && $d > 0) {
                    $year = $y + 1;
                } else{
                    $year = $y;
                }

                $i = $data_json['tenor'];
                if (!empty($i)) {
                    $yearss = $i/12;
                    $pembulatan = number_format((float)$yearss, 1);
                    $yea = substr(strrchr($pembulatan, "."), 1);
                        if ($yea > 6) {
                            $years = floor($pembulatan) + 1;
                        } else{
                            $years = floor($pembulatan);
                        }
                } else{
                    $years = '0';
                }

                $usiamax = (int)$years + (int)$year;


                $tahunlahir = substr($ab,0,4);
                //Error tahun lahir tidak boleh tahun ini
                if($tahunlahir == strval($y)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '25', "message"=>"Tahun Lahir tidak mungkin tahun ini"));
                        echo json_encode($return);
                        die;
                }

                //Error jika data under writting tidak ditemukan

                //Innisiai variable rate & under writting
                $medcheck = $this->db->query("select a.id_pekerjaan, c.id_type, a.kode_jenisdeb, b.status,b.usia_max FROM PAN_BRK.dbo.MasterDebitur a
                left join PAN_BRK.dbo.MedicalCheckup b on b.kode_jenisdeb = a.kode_jenisdeb
                left join PAN_BRK.dbo.TypeManfaat c on c.kode_jenisdeb = a.kode_jenisdeb
                where a.id_pekerjaan = '$id' and
                '$amount' between b.plafon_min and b.plafon_max
                and '$year' between b.usia_min and b.usia_max and
                benefit = '$benefit'
                group by a.kode_jenisdeb, b.status, a.id_pekerjaan, c.id_type, b.usia_max")->result_array();

                $plans = $data_json['jenis_pembiayaan'];
                $plafond = $data_json['plafond'];
                if ($plans == '326' || $plans == '358') {
                    $kode = 'INTERNALBANK';
                } 
                 else{
                    if (!empty($medcheck[0]['kode_jenisdeb'])) {
                        $kode = $medcheck[0]['kode_jenisdeb'];
                    } else{
                        $kode = '';
                        if(empty($kode)){
                            $return = array("Result"=>array('status' => '200', 'kode_response' => '16', "message"=>"Underwriting tidak ditemukan untuk uang pertanggungan: '$plafond', Usia: '$year' dan Dengan Benefit Terkait"));
                                echo json_encode($return);
                                die;
                        }
                    }
                    
                }

                if ($kode == 'PENSIUNAN' || $year >= 55) {
                    $idtypes = '13';
                } elseif ($kode == 'INTERNALBANK') {
                    $id = '43';
                } else{
                    if (!empty($medcheck[0]['id_type'])) {
                        $idtypes = $medcheck[0]['id_type'];
                    } else{
                        $idtypes = '';
                    }
                    
                }

                $type = $this->db->query("select * from PAN_BRK.dbo.Rate a
                join PAN_BRK.dbo.TypeManfaat b on a.id_type = b.id_type
                join PAN_BRK.dbo.MasterDebitur c on c.kode_jenisdeb = b.kode_jenisdeb
                join PAN_BRK.dbo.MedicalCheckup d on d.kode_jenisdeb = c.kode_jenisdeb
                where c.kode_jenisdeb = '$kode' and a.jangka_waktu = '$years' and a.id_type = '$idtypes' and c.id_pekerjaan = '$id' 
                and '$amount' between d.plafon_min and d.plafon_max and '$year' between d.usia_min and d.usia_max")->result_array();

                if (empty($type) || $type == '' || $type == null) {
                    $med = 'CBC';
                    $rate = '0'; 
                } else{
                    $med = $type[0]['status'];
                    $rate = $type[0]['rate'];
                }

                if ($med == 'CAC') {
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '06', "message"=>"Usia dan plafond tidak termasuk kategori CBC"));
                            echo json_encode($return);
                            die;
                } else{
                    $cbc = $this->db->query("select * from PAN_BRK.dbo.MedicalCheckup
                where '$amount' between plafon_min and plafon_max and status = '$med'")->result_array();
                    if(empty($cbc)){
                        $return = array("Result"=>array('status' => '200', 'kode_response' => '07', "message"=>"Plafond dibawah kategori CBC"));
                            echo json_encode($return);
                            die;
                    }
                }

                $usiamax_pelunasan = $medcheck[0]['usia_max'];
                if((int)$usiamax > (int)$usiamax_pelunasan){
                    $return = array("Result"=>array('status' => '400', 'kode_response' => '18', "message"=>"Usia Sekarang '$year', Usia saat pelunasan '$usiamax' melebihi batas maksimal umur saat pelunasan,yaitu: '$usiamax_pelunasan'"));
                            echo json_encode($return);
                            die;
                }

                $ben = $data_json['benefit'];
                if(empty($ben)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '32', "message"=>"Benefit Kosong"));
                    echo json_encode($return);
                    die;
                }

                $premis = ($plafond * $rate) / 1000;

                $arr_insert = array(
                        'id_transaksi' => $data_json['id_transaksi'],
                        'id_pengajuan' => $data_json['id_pengajuan'],
                        'kode_broker' => $data_json['kode_broker'],
                        'cab' => $data_json['kode_cabang'],
                        'ktp' => $data_json['ktp'],
                        'sex' => $data_json['jenis_kelamin'],
                        'nama' => $data_json['nama'],
                        'lahir' => $data_json['tgl_lahir'],
                        'id' => $data_json['pekerjaan'],
                        'plan' => $data_json['jenis_pembiayaan'],
                        'amount' => $data_json['plafond'],
                        'tempo' => $data_json['tenor'],
                        'benefit' => $data_json['benefit'],
                        'date_modified' => date("Y-m-d H:i:s"),
                    );

                    $insert = $this->db->query("update Tampung_Riau.dbo.DataRiau set 
                    id_transaksi = '".$arr_insert['id_transaksi']."', 
                    id_pengajuan = '".$arr_insert['id_pengajuan']."',
                    kode_broker = '".$arr_insert['kode_broker']."',
                    cab = '".$arr_insert['cab']."',
                    ktp = '".$arr_insert['ktp']."',
                    sex = '".$arr_insert['sex']."',
                    nama = '".$arr_insert['nama']."',
                    lahir = '".$arr_insert['lahir']."',
                    id = '".$arr_insert['id']."',
                    plankredit = '".$arr_insert['plan']."',
                    amount = '".$arr_insert['amount']."',
                    tempo = '".$arr_insert['tempo']."',
                    benefit = '".$arr_insert['benefit']."',
                    date_modified = '".$arr_insert['date_modified']."'
                    where id_pengajuan = '".$arr_insert['id_pengajuan']."'");

                 if($insert === false){ 
                    $success = false;
                 }
            }

            if($success === true){

                $arr_insert = array(
                    'id_transaksi' => $data_json['id_transaksi'],
                    'id_pengajuan' => $data_json['id_pengajuan'],
                    'kode_broker' => $data_json['kode_broker'],
                    'cab' => $data_json['kode_cabang'],
                    'ktp' => $data_json['ktp'],
                    'sex' => $data_json['jenis_kelamin'],
                    'nama' => $data_json['nama'],
                    'lahir' => $data_json['tgl_lahir'],
                    'id' => $data_json['pekerjaan'],
                    'plan' => $data_json['jenis_pembiayaan'],
                    'amount' => $data_json['plafond'],
                    'tempo' => $data_json['tenor'],
                    'benefit' => $data_json['benefit'],
                    'date_modified' => date("Y-m-d H:i:s"),
                    );

                    $ch = curl_init();
                    $data_string = json_encode($arr_insert);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                    // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/GetCBC");
                    curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupandev/GetCBC");
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    // curl_setopt($ch, CURLOPT_PORT, 888);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    $result = curl_exec($ch);

                    curl_close($ch);

                    
                    echo $httpcode;
                    if($httpcode == '200'){
                        $json = json_decode($result, true);

                        if($json['status'] == '200'){
                                $this->db->query("update Tampung_Riau.dbo.DataRiau set stcurl = '1' where norek = '$norek'");
                        }
                        }

                $sqlget = $this->db->query("select * from Tampung_Riau.dbo.DataRiau 
                where id_transaksi = '".$arr_insert['id_transaksi']."' and id_pengajuan = '".$arr_insert['id_pengajuan']."' ")->row();
               
                $insertScheduler = $this->db->query("insert into Tampung_Riau.dbo.Scheduler_CBC (
                    id_data,
                    ktp,
                    pk,
                    norek,
                    version,
                    status
                    ) 
                    values (
                    '".$sqlget->id_data."',
                    '".$sqlget->pk."',
                    '".$sqlget->norek."',
                    '".$arr_insert['ktp']."',
                    '1',
                    '0'
                    )");
   
                    if($insertScheduler === false){ 
                       $success = false;
                    }

                    $return = array("Result"=>array(
                        'status' => '200',
                        'kode_response' => '00',
                        'message' => 'Berhasil kirim pengajuan dokumen CBC',
                        'status_dokumen' => '1',
                        'premi_disetujui' => $premis,
                        'keterangan' => ''
                    ));

            echo json_encode($return, JSON_UNESCAPED_SLASHES);
            die;

            } else{

                $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error lainnya"));
                echo json_encode($return);
                die;

            }
        
    }

    public function kirimdata(){

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where stcurl = 0 ")->result_array();

                if (!empty($sql)) {
                        foreach ($sql as $key => $value) {

                        $arr_insert = array(
                        'id_transaksi' => $value['id_transaksi'],
                        'id_pengajuan' => $value['id_pengajuan'],
                        'kode_broker' => $value['kode_broker'],
                        'cab' => $value['kode_cabang'],
                        'ktp' => $value['ktp'],
                        'sex' => $value['jenis_kelamin'],
                        'nama' => $value['nama'],
                        'lahir' => $value['tgl_lahir'],
                        'id' => $value['pekerjaan'],
                        'plan' => $value['plankredit'],
                        'amount' => $value['plafond'],
                        'tempo' => $value['tenor'],
                        'benefit' => $value['benefit'],
                        'date_modified' => date("Y-m-d H:i:s"),
                        );

                        $ch = curl_init();
                        $data_string = json_encode($arr_insert);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/GetCBC");
                        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupandev/GetCBC");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        // curl_setopt($ch, CURLOPT_PORT, 888);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            echo $httpcode;
                            if($httpcode == '200'){
                                $json = json_decode($result, true);

                             if($json['status'] == '200'){
                                     $this->db->query("update Tampung_Riau.dbo.DataRiau set stcurl = '1' where norek = '$norek'");
                             }
                             }
                         }
                    
                }
    }

}
