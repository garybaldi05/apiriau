<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class get_token extends CI_Controller {

    private $secretkey = 'Password;;';

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        // $a = $this->input->raw_input_stream;
        // var_dump($a);
        // die();
        $this->generate_post();
    }

    public function generate_post(){

        $data = $this->input->raw_input_stream;
        // var_dump($data);
        // die();
        $data_json = json_decode($data, true);

        $success = true;

            $clientid = $data_json['client_id'];
            $client_secret = $data_json['client_secret'];
            $username = $data_json['username'];
            $password = $data_json['password'];
            $grand_type = $data_json['grand_type'];
            $scope = $data_json['scope'];

            if ($clientid == '' || $client_secret == '' || $username == '' || $password == '' || $grand_type == '' || $scope == '') {
            $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error koneksi atau data"));
            echo json_encode($return);
            die;
            }     
            else if ($clientid != 'clientidBRK' || $client_secret != 'clientsecretBRK' || $username != 'usernameBRK' || $password != 'passwordBRK' || $grand_type != 'password credential' || $scope != 'roles') {
                $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error koneksi atau data"));
                echo json_encode($return);
                die;
            }

            if(!empty($data_json)){

                        $token = openssl_random_pseudo_bytes(25);
                        $token = bin2hex($token);

                        date_default_timezone_set('Asia/Jakarta');
                        $datenow = date("Y-m-d H:i:s");
                        $dateplus = date("Y-m-d 23:59:59");

                            $arr_insert = array(
                                'client_id' => $data_json['client_id'],
                                'client_secret' => $data_json['client_secret'],
                                'username' => $data_json['username'],
                                'password' => $data_json['password'],
                                'grand_type' => $data_json['grand_type'],
                                'scope' => $data_json['scope'],
                                'token' => $token,
                                'date_created' => $datenow,
                                'date_expired' => $dateplus,
                            );
                            // var_dump($arr_insert);
                            // die();
                            $insert = $this->db->query("insert into Tampung_Riau.dbo.Get_Token (
                            client_id,
                            client_secret,
                            username,
                            password,
                            grand_type,
                            scope,
                            token,
                            date_created,
                            date_expired
                            ) 
                            values (
                            '".$arr_insert['client_id']."',
                            '".$arr_insert['client_secret']."',
                            '".$arr_insert['username']."',
                            '".$arr_insert['password']."',
                            '".$arr_insert['grand_type']."',
                            '".$arr_insert['scope']."',
                            '".$arr_insert['token']."',
                            '".$arr_insert['date_created']."',
                            '".$arr_insert['date_expired']."'
                            )");

                            if($insert === false) $success = false;
                       
                    if($success){

                        $return = array("Result"=>array('status' => '200', 'kode_response' => '00', "message"=>"Request Berhasil", "token" => $token));

                        echo json_encode($return);
                        die;
                    }else{
                        $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error koneksi atau data"));
                        echo json_encode($return);
                        die;
                    }
                } else{
                        $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error koneksi atau data"));
                        echo json_encode($return);
                        die;
                    }
                }

}
