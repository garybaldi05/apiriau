<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class summary extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        $this->getdatariau();
    }

    public function getdatariau(){

        $data = $this->input->raw_input_stream;

        $data_json = json_decode($data, true);

        $success = true;

        $jwt = $this->input->get_request_header('Authorization');

        $token = null;
 
        if(!empty($jwt)) {
            if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                $token = $matches[1];
            }
        }

        if(is_null($token) || empty($token)) {
            $return = array("Result"=>array('status' => '400', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

        $cek_token = $this->db->query("select * from Tampung_Riau.dbo.Get_Token where CONVERT(VARCHAR(MAX), token) = '$token' ")->result_array();

        date_default_timezone_set('Asia/Jakarta');

        if ( strtotime(date("Y-m-d H:i:s")) > strtotime($cek_token[0]['date_expired']) ) {
        $return = array("Result"=>array('status' => '400', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

            if ($data_json) {
                date_default_timezone_set('Asia/Jakarta');
                $arr_insert = array(
                        'id_transaksi' => $data_json['id_transaksi'],
                        'kode_broker' => $data_json['kode_broker'],
                        'kode_cabang' => $data_json['kode_cabang'],
                        'periode_awal' => $data_json['periode_awal'],
                        'periode_akhir' => $data_json['periode_akhir']
                        
                    );

                    $id_transaksi = $data_json['id_transaksi'];

                    
                    $dataresitusi = "select * from Tampung_Riau.dbo.DataRiau
                    where id_transaksi = '$id_transaksi'";
                    

                    if(empty($dataresitusi)){
                        $return = array("Result"=>array('status' => '400', 'kode_response' => '99', "message"=>"Error lainnya"));
                        echo json_encode($return);
                        die;
                    }
                    
                    $polis1 = $dataresitusi[0];
                    $awal = date("Ymd", strtotime($polis1['periode_awal']));
                    $akhir = date("Ymd", strtotime($polis1['periode_akhir']));
                    $norek = $polis1['norek'];

                    $datapolis = "select * from Tampung_Riau.dbo.Input_Polis
                    where nomor_rekening = '$norek'";
                    $polis2 = $datapolis[0];

                    $dataklaim = "select * from Tampung_Riau.dbo.Pengajuan_Klaim
                    where id_transaksi = '$id_transaksi'";
                    $polis3 = $dataklaim[0];

                    $sk = preg_replace( "/\r|\n/", " ", $polis1['s&k']);
                    $return = array("Result"=>array(
                        'status' => '200',
                        'kode_response' => '00',
                        'message' => 'Berhasil kirim Summary Broker',
                        'kode_broker' => $polis1['kode_broker'],
                        'kode_cabang' => $polis1['cab'],
                        'periode_awal' => $awal,
                        'periode_akhir' => $akhir,
                        'debitur' => array(
                            'id_transaksi' => $polis1['id_transaksi'],
                            'nama' => $polis1['nama'],
                            'ktp' => $polis1['ktp'],
                            'kode_cabang' => $polis1['cab'],
                            'kode_broker' => $polis1['kode_broker'],
                            'nomor_rekening' => $polis1['nomor_rekening'],
                            'no_pk' => $polis1['pk'],
                            'polis' => array(
                                'id_transaksi_pengajuan' => $polis2['id_polis'],
                                'jenis_pengajuan' => $polis2['jenis_pengajuan'],
                                'asuransi' => $polis2['asuransi'],
                                'jenis_penjaminan' => $polis2['jenis_penjaminan'],
                                'no_polis' => $polis2['no_polis'],
                                's&k' => $sk,
                                'periode_awal' => $awal,
                                'periode_akhir' => $akhir,
                                'nilai_penjaminan' => $polis2['nilai_penjaminan'],
                                'tarif_imbal_jasa' => $polis2['tarif_imbal_jasa'],
                                'jumlah_imbal_jasa' => $polis2['jumlah_imbal_jasa'],
                                'tarif_extra_premi' => $polis2['tarif_extra_premi'],
                                'jumlah_extra_premi' => $polis2['jumlah_extra_premi'],
                                'sertifikat' => $polis2['link_sertifikat'],
                                'status_akseptasi' => '5',
                                'keterangan' => ''
                            ),
                            'resitusi' => array(
                                'id_transaksi_pengajuan' => $polis1['id_polis'],
                                'status_pengajuan' => $polis1['status_pangajuan'],
                                'tenor' => $polis1['tenor'],
                                'premi' => $polis1['premi'],
                                'periode_awal' => $awal,
                                'periode_akhir' => $akhir,
                                'tenor_berjalan' => $polis1['tenor_berjalan'],
                                'sisa_tenor' => $polis1['sisa_tenor'],
                                'status_bayar' => $polis1['status_bayar'],
                                'premi_dikembalikan' => $polis1['premi_dikembalikan'],
                                'asuransi' => $polis1['asuransi']
                            ),
                            'klaim' => array(
                                'id_transaksi_pengajuan' => $polis3['id_polis'],
                                'status_pengajuan' => $polis3['status_pangajuan'],
                                'status_bayar' => $polis3['status_bayar'],
                                'klaim_dibayarkan' => $polis3['klaim_dibayarkan'],
                                'asuransi' => $polis3['asuransi'],
                                'keterangan' => $polis3['keterangan']
                            ),
                        )
                    ));

             
                    echo json_encode($return, JSON_UNESCAPED_SLASHES);
                    die;

            
            }else{
                $return = array("Result"=>array('status' => '400', 'kode_response' => '99', "message"=>"Error lainnya"));
                echo json_encode($return);
                die;
            }
        
    }
























    //     $kodeh2h = $data_json['kodeh2h'];
    //     $username = $data_json['username'];
    //     $password = $data_json['password'];

      
    //     if ($kodeh2h == '' || $username == '' || $password == '') {
    //         //$connection = $this->db->get('cek_komunikasi')->result();
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }     
    //     else if ($kodeh2h != '633e18b8cf68b8e2d1d5fbc6ab30deaf06b0e' || $username != 'brk' || $password != 'brk!')
    //     {
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }    
    //     else {
    //         date_default_timezone_set('Asia/Jakarta');
    //         $stcurl = '0';
    //         // foreach($data_json as $idx => $dt){
    //             $arr_insert = array(
    //                 'kodeh2h' => $data_json['kodeh2h'],
    //                 'username' => $data_json['username'],
    //                 'password' => $data_json['password'],
    //                 'cab' => $data_json['cab'],
    //                 'pk' => $data_json['pk'],
    //                 'norek' => $data_json['norek'],
    //                 'nama' => $data_json['nama'],
    //                 'lahir' => $data_json['lahir'],
    //                 'buka' => $data_json['buka'],
    //                 'tempo' => $data_json['tempo'],
    //                 'plankredit' => $data_json['plan'],
    //                 'amount' => $data_json['amount'],
    //                 'id' => $data_json['id'],
    //                 'ktp' => $data_json['ktp'],
    //                 'rate' => $data_json['rate'],
    //                 'sex' => $data_json['sex'],
    //                 'rate_asuransi' => $data_json['rate_asuransi'],
    //                 'asuransi' => $data_json['asuransi'],
    //                 'npwp' => $data_json['npwp'],
    //                 'old_pk' => $data_json['old_pk'],
    //                 'date_created' => date("Y-m-d H:i:s"),
    //                 'date_modified' => date("Y-m-d H:i:s"),
    //                 'stcurl' => $stcurl,
    //             );
    //             // var_dump($arr_insert);
    //             // die();
    //             $insert = $this->db->query("insert into Tampung_Riau.dbo.DataRiau (
    //             kodeh2h,
    //             userid,
    //             password,
    //             cab,
    //             pk,
    //             norek,
    //             nama,
    //             lahir,
    //             buka,
    //             tempo,
    //             plankredit,
    //             amount,
    //             id,
    //             ktp,
    //             rate,
    //             sex,
    //             rate_asuransi,
    //             asuransi,
    //             npwp,
    //             old_pk,
    //             date_created,
    //             date_modified,
    //             stcurl
    //             ) 
    //             values (
    //             '".$arr_insert['kodeh2h']."',
    //             '".$arr_insert['username']."',
    //             '".$arr_insert['password']."',
    //             '".$arr_insert['cab']."',
    //             '".$arr_insert['pk']."',
    //             '".$arr_insert['norek']."',
    //             '".$arr_insert['nama']."',
    //             '".$arr_insert['lahir']."',
    //             '".$arr_insert['buka']."',
    //             '".$arr_insert['tempo']."',
    //             '".$arr_insert['plankredit']."',
    //             '".$arr_insert['amount']."',
    //             '".$arr_insert['id']."',
    //             '".$arr_insert['ktp']."',
    //             '".$arr_insert['rate']."',
    //             '".$arr_insert['sex']."',
    //             '".$arr_insert['rate_asuransi']."',
    //             '".$arr_insert['asuransi']."',
    //             '".$arr_insert['npwp']."',
    //             '".$arr_insert['old_pk']."',
    //             '".$arr_insert['date_created']."',
    //             '".$arr_insert['date_modified']."',
    //             '".$arr_insert['stcurl']."'
    //             )");
    //             // $insert = $this->db->insert('DataRiau', $arr_insert);

    //             if($insert === false) $success = false;
    //         }
    //     }

    //     if($success){

            

    //         // $ch = curl_init();
    //             // $data_string = json_encode($data_json);
    //         //     curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
    //         //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    //         //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    //         //     // curl_setopt($ch, CURLOPT_PORT, 888);
    //         //     curl_setopt($ch, CURLOPT_POST, true);
    //         //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //         //     // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //         //     // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //         //     curl_exec($ch);
    //         //     curl_close($ch);

    //             // if(curl_errno($ch)){
    //             //     $err = curl_error($ch);
    //             //     echo $err;
    //             // }


    //         $return = array("Result"=>array('status' => '200', "message"=>"Data sudah di input"));

    //         echo json_encode($return);
    //         die;
    //     }else{
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }
    // }

    public function kirimdata(){

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where stcurl = 0 ")->result_array();

        // var_dump($sql);
        // die();

                if (!empty($sql)) {
                        $asuransi = '';

                        foreach ($sql as $key => $value) {

                        $norek = $value['norek'];
                        $arr_insert = array(
                        'id_transaksi' => $value['id_transaksi'],
                        'kode_broker' => $value['kode_broker'],
                        'cab' => $value['cab'],
                        'pk' => $value['pk'],
                        'norek' => $value['norek'],
                        'nama' => $value['nama'],
                        'lahir' => $value['lahir'],
                        'buka' => $value['buka'],
                        'tempo' => $value['tempo'],
                        'plan' => $value['plankredit'],
                        'amount' => $value['amount'],
                        'id' => $value['id'],
                        'ktp' => $value['ktp'],
                        'rate' => $value['rate'],
                        'sex' => $value['sex'],
                        'rate_asuransi' => $value['rate'],
                        'asuransi' => $asuransi,
                        'npwp' => $value['npwp'],
                        'old_pk' => $value['old_pk'],
                        'benefit' => $value['benefit'],

                        );
                    }

                        $ch = curl_init();
                        $data_string = json_encode($arr_insert);
                        // curl_setopt($ch, CURLOPT_HEADER, false);
                        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        // curl_setopt($ch, CURLOPT_PORT, 888);
                        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                        // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_exec($ch);
                        curl_close($ch);

                        // if(curl_errno($ch)){
                        //     $err = curl_error($ch);
                        //     echo $err;
                        // }

                        $this->db->query("update Tampung_Riau.dbo.DataRiau set stcurl = '1' where norek = '$norek'");
                    
                }
    }

}
