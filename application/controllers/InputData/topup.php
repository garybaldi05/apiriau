<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class topup extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // ini_set('max_execution_time', 0);
        // set_time_limit(0);

    }

    public function index()
    {

        $this->getdatariau();
    }

    public function getdatariau(){

        $data = $this->input->raw_input_stream;

        $data_json = json_decode($data, true);

        $success = true;

        $jwt = $this->input->get_request_header('Authorization');

        $token = null;
 
        if(!empty($jwt)) {
            if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                $token = $matches[1];
            }
        }

        if(is_null($token) || empty($token)) {
            $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

        $cek_token = $this->db->query("select * from Tampung_Riau.dbo.Get_Token where CONVERT(VARCHAR(MAX), token) = '$token' ")->result_array();

        date_default_timezone_set('Asia/Jakarta');

        if ( strtotime(date("Y-m-d H:i:s")) > strtotime($cek_token[0]['date_expired']) ) {
        $return = array("Result"=>array('status' => '200', 'kode_response' => '05', "message"=>"Token tidak sesuai"));
                echo json_encode($return);
                die;
        }

            if ($data_json) {
                date_default_timezone_set('Asia/Jakarta');
                $stcurl = '0';

                $amount = $data_json['plafond'];
                $ab = $data_json['tanggal_lahir'];
                $id = $data_json['pekerjaan'];
                // $oldpk1 = $arr_data['old_pk1'];
                $tanggal = new DateTime($ab);
                $today = new DateTime('today');
                $y = $today->diff($tanggal)->y;
                $m = $today->diff($tanggal)->m;
                $d = $today->diff($tanggal)->d;
                if ($m >= 6 && $d > 0) {
                    $year = $y + 1;
                } else{
                    $year = $y;
                }

                $i = $data_json['tenor'];
                if (!empty($i)) {
                    $yearss = $i/12;
                    $pembulatan = number_format((float)$yearss, 1);
                    $yea = substr(strrchr($pembulatan, "."), 1);
                        if ($yea > 6) {
                            $years = floor($pembulatan) + 1;
                        } else{
                            $years = floor($pembulatan);
                        }
                } else{
                    $years = '0';
                }

                $usiamax = (int)$years + (int)$year;


                $tahunlahir = substr($ab,0,4);
                //Error tahun lahir tidak boleh tahun ini
                if($tahunlahir == strval($y)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '25', "message"=>"Tahun Lahir tidak mungkin tahun ini"));
                        echo json_encode($return);
                        die;
                }
                //Error jika data pekerjaan tidak ditemukan
                $id_pekerjaan = $data_json['pekerjaan'];
                $cek_datapekerjaan = $this->db->query("select * from Tampung_Riau.dbo.MasterDebitur where id_pekerjaan = '$id_pekerjaan' ")->result_array();
                if(empty($cek_datapekerjaan)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '12', "message"=>"Data Pekerjaan Error / Data Pekerjaan Tidak Ditemukan"));
                        echo json_encode($return);
                        die;
                }
                //Error jika data plan tidak ditemukan
                $id_plan = $data_json['jenis_pembiayaan'];
                $plafond = $data_json['plafond'];
                $cek_dataplan = $this->db->query("select * from Tampung_Riau.dbo.MasterPlan where kode_plan = '$id_plan' ")->result_array();
                if(empty($cek_dataplan)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '15', "message"=>"Kode Plan Tidak Ditemukan / Uang Pertanggungan: '$plafond' Melebihi Ketentuan"));
                        echo json_encode($return);
                        die;
                }


                //Error jika data under writting tidak ditemukan

                //Innisiai variable rate & under writting
                $medcheck = $this->db->query("select a.id_pekerjaan, c.id_type, a.kode_jenisdeb, b.status,b.usia_max FROM PAN_BRK.dbo.MasterDebitur a
                left join PAN_BRK.dbo.MedicalCheckup b on b.kode_jenisdeb = a.kode_jenisdeb
                left join PAN_BRK.dbo.TypeManfaat c on c.kode_jenisdeb = a.kode_jenisdeb
                where a.id_pekerjaan = '$id' and
                '$amount' between b.plafon_min and b.plafon_max
                and '$year' between b.usia_min and b.usia_max
                group by a.kode_jenisdeb, b.status, a.id_pekerjaan, c.id_type, b.usia_max")->result_array();

                $plans = $data_json['jenis_pembiayaan'];
                $oldpk = $data_json['old_nomor_akad'];
                if ($plans == '326' || $plans == '358') {
                    $kode = 'INTERNALBANK';
                } 
                 else{
                    if (!empty($medcheck[0]['kode_jenisdeb'])) {
                        $kode = $medcheck[0]['kode_jenisdeb'];
                    } else{
                        $kode = '';
                        if(empty($kode)){
                            $return = array("Result"=>array('status' => '400', 'kode_response' => '16', "message"=>"Underwriting tidak ditemukan untuk uang pertanggungan: '$plafond', Usia: '$year' dan Dengan Benefit Terkait"));
                                echo json_encode($return);
                                die;
                        }
                    }
                    
                }

                if ($kode == 'PENSIUNAN' || $year >= 55) {
                    $idtypes = '13';
                } elseif ($kode == 'UMUM01') {
                    $idtypes = '1';
                } elseif ($kode == 'KHUSUS01') {
                    $idtypes = '2';
                } elseif ($kode == 'INTERNALBANK') {
                    $idtypes = '6';
                    $id = '43';
                } elseif ($kode == 'DEWAN') {
                    $idtypes = '4';
                } elseif ($kode == 'UMUM01TOPUP') {
                    $idtypes = '5';
                    $id = '42';
                } else{
                    if (!empty($medcheck[0]['id_type'])) {
                        $idtypes = $medcheck[0]['id_type'];
                    } else{
                        $idtypes = '';
                    }
                    
                }

                $type = $this->db->query("select * from PAN_BRK.dbo.Rate a
                join PAN_BRK.dbo.TypeManfaat b on a.id_type = b.id_type
                join PAN_BRK.dbo.MasterDebitur c on c.kode_jenisdeb = b.kode_jenisdeb
                join PAN_BRK.dbo.MedicalCheckup d on d.kode_jenisdeb = c.kode_jenisdeb
                where c.kode_jenisdeb = '$kode' and a.jangka_waktu = '$years' and a.id_type = '$idtypes' and c.id_pekerjaan = '$id' and '$amount' between d.plafon_min and d.plafon_max and '$year' between d.usia_min and d.usia_max")->result_array();

                $tenor = $data_json['tenor'];
                if (empty($type) || $type == '' || $type == null) {
                    $med = 'CBC';
                    $rate = '0'; 
                    //Error jika data rate tidak ditemukan
                    if($rate == '0'){
                        $return = array("Result"=>array('status' => '200', 'kode_response' => '17', "message"=>"Rate tidak ditemukan untuk tempo: '$tenor', uang pertanggungan:'$plafond', Usia: '$year', dan Dengan Benefit Terkait"));
                            echo json_encode($return);
                            die;
                    }
                } else{
                    $med = $type[0]['status'];
                    $rate = $type[0]['rate'];
                }

                $usiamax_pelunasan = $medcheck[0]['usia_max'];
                if((int)$usiamax > (int)$usiamax_pelunasan){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '18', "message"=>"Usia Sekarang '$year', Usia saat pelunasan '$usiamax' melebihi batas maksimal umur saat pelunasan,yaitu: '$usiamax_pelunasan'"));
                            echo json_encode($return);
                            die;
                }

                $premis = ($type[0]['amount'] * $type[0]['rate_asuransi']) / 1000;
                $premi_json = $data_json['premi_yang_dibayarkan'];

                if($premis != (int)$premi_json){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '19', "message"=>"Premi Tidak Sesuai"));
                            echo json_encode($return);
                            die;
                }

                $norek = $data_json['nomor_rekening'];
                $pk = $data_json['nomor_akad'];
                $polis = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where nomor_rekening = '$norek' and nomor_akad = $pk ")->result_array();

                if(!empty($polis)){
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '26', "message"=>"Terdapat Akumulasi Pinjaman Terhadap Nasabah Ini"));
                    echo json_encode($return);
                    die;
                }

                $arr_insert = array(
                        'id_transaksi' => $data_json['id_transaksi'],
                        'kode_broker' => $data_json['kode_broker'],
                        'cab' => $data_json['kode_cabang'],
                        'pk' => $data_json['nomor_akad'],
                        'norek' => $data_json['nomor_rekening'],
                        'nama' => $data_json['nama'],
                        'ktp' => $data_json['ktp'],
                        'npwp' => $data_json['npwp'],
                        'sex' => $data_json['jenis_kelamin'],
                        'id' => $data_json['pekerjaan'],
                        'lahir' => $data_json['tgl_lahir'],
                        'buka' => $data_json['tgl_buka'],
                        'tempo' => $data_json['tenor'],
                        'plan' => $data_json['jenis_pembiayaan'],
                        'amount' => $data_json['plafond'],                        
                        'rate' => $data_json['bunga'],
                        'rate_asuransi' => $data_json['bunga'],
                        'premi_bayar' => $data_json['premi_yang_dibayarkan'],
                        'benefit' => $data_json['benefit'],
                        'old_pk' => $data_json['old_nomor_akad'],
                        'tanggal_akad_lama' => $data_json['tanggal_akad_lama'],
                        'plafond_lama' => $data_json['plafond_lama'],
                        'tanggal_pengajuan_restitusi' => $data_json['tanggal_pengajuan_restitusi'],
                        'tenor_lama' => $data_json['tenor_lama'],
                        'tenor_berjalan' => $data_json['tenor_berjalan'],
                        'sisa_tenor' => $data_json['sisa_tenor'],
                        'premi_lama' => $data_json['premi_lama'],
                        'asuransi_lama' => $data_json['asuransi_lama'],
                        'tujuan_pembayaran' => $data_json['tujuan_pembayaran'],
                        'date_created' => date("Y-m-d H:i:s"),
                        'date_modified' => date("Y-m-d H:i:s"),
                        'stcurl' => $stcurl,
                    );

                $insert = $this->db->query("insert into Tampung_Riau.dbo.DataRiau (
                 id_transaksi,
                 kode_broker,
                 cab,
                 pk,
                 norek,
                 nama,
                 lahir,
                 buka,
                 tempo,
                 plankredit,
                 amount,
                 id,
                 ktp,
                 rate,
                 sex,
                 rate_asuransi,
                 npwp,
                 old_pk,
                 tanggal_akad_lama,
                 plafond_lama,
                 tenor_lama,
                 date_created,
                 date_modified,
                 benefit,
                 premi_bayar,
                 tanggal_pengajuan_restitusi,
                 tenor_berjalan,
                 sisa_tenor,
                 premi_lama,
                 asuransi_lama,
                 tujuan_pembayaran,
                 stcurl
                 ) 
                 values (
                 '".$arr_insert['id_transaksi']."',
                 '".$arr_insert['kode_broker']."',
                 '".$arr_insert['cab']."',
                 '".$arr_insert['pk']."',
                 '".$arr_insert['norek']."',
                 '".$arr_insert['nama']."',
                 '".$arr_insert['lahir']."',
                 '".$arr_insert['buka']."',
                 '".$arr_insert['tempo']."',
                 '".$arr_insert['plan']."',
                 '".$arr_insert['amount']."',
                 '".$arr_insert['id']."',
                 '".$arr_insert['ktp']."',
                 '".$arr_insert['rate']."',
                 '".$arr_insert['sex']."',
                 '".$arr_insert['rate_asuransi']."',
                 '".$arr_insert['npwp']."',
                 '".$arr_insert['old_pk']."',
                 '".$arr_insert['tanggal_akad_lama']."',
                 '".$arr_insert['plafond_lama']."',
                 '".$arr_insert['tenor_lama']."',
                 '".$arr_insert['date_created']."',
                 '".$arr_insert['date_modified']."',
                 '".$arr_insert['benefit']."',
                 '".$arr_insert['premi_bayar']."',
                 '".$arr_insert['tanggal_pengajuan_restitusi']."',
                 '".$arr_insert['tenor_berjalan']."',
                 '".$arr_insert['sisa_tenor']."',
                 '".$arr_insert['premi_lama']."',
                 '".$arr_insert['asuransi_lama']."',
                 '".$arr_insert['tujuan_pembayaran']."',
                 '".$arr_insert['stcurl']."'
                 )");

                 if($insert === false){ 
                    $success = false;
                 }
            }

            if($success === true){

                $norek = $data_json['nomor_rekening'];
                $pk = $data_json['nomor_akad'];
                $polis = $this->db->query("select * from Tampung_Riau.dbo.Input_Polis where nomor_rekening = '$norek' and nomor_pk = '$pk' ")->result_array();

                $polis1 = $polis[0];
                $awal = date("Ymd", strtotime($polis1['periode_awal']));
                $akhir = date("Ymd", strtotime($polis1['periode_akhir']));

                $sk = preg_replace( "/\r|\n/", " ", $polis1['s&k']);

                // var_dump($a);
                // die();
                $topup = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where pk = '$oldpk'")->result_array();

                if (!empty($topup)) {
                   
                    if (!empty($polis)) {
                    if($med == 'CAC'){
                    $return = array("Result"=>array(
                        'status' => '200',
                        'kode_response' => '00',
                        'message' => 'Berhasil kirim pengajuan polis asuransi',
                        'nama' => $polis1['nama'],
                        'nomor_rekening' => $polis1['nomor_rekening'],
                        'nomor_pk' => $polis1['nomor_pk'],
                        'jenis_pengajuan' => '1',
                        'asuransi' => $polis1['asuransi'],
                        'jenis_penjaminan' => $polis1['jenis_penjaminan'],
                        'no_polis' => $polis1['no_polis'],
                        's&k' => $sk,
                        'periode_awal' => $awal,
                        'periode_akhir' => $akhir,
                        'nilai_penjaminan' => $polis1['nilai_penjaminan'],
                        'tarif_imbal_jasa' => $polis1['tarif_imbal_jasa'],
                        'jumlah_imbal_jasa' => $polis1['jumlah_imbal_jasa'],
                        'tarif_extra_premi' => $polis1['tarif_extra_premi'],
                        'jumlah_extra_premi' => $polis1['jumlah_extra_premi'],
                        '“status_akseptasi”' => '5',
                        'keterangan' => '-'
                    ));
                    }else{
                        $return = array("Result"=>array(
                            'status' => '200',
                            'kode_response' => '00',
                            'message' => 'Berhasil kirim pengajuan polis asuransi',
                            'nama' => $polis1['nama'],
                            'nomor_rekening' => $polis1['nomor_rekening'],
                            'nomor_pk' => $polis1['nomor_pk'],
                            'jenis_pengajuan' => '2',
                            'asuransi' => $polis1['asuransi'],
                            'jenis_penjaminan' => $polis1['jenis_penjaminan'],
                            'no_polis' => $polis1['no_polis'],
                            's&k' => $sk,
                            'periode_awal' => $awal,
                            'periode_akhir' => $akhir,
                            'nilai_penjaminan' => $polis1['nilai_penjaminan'],
                            'tarif_imbal_jasa' => $polis1['tarif_imbal_jasa'],
                            'jumlah_imbal_jasa' => $polis1['jumlah_imbal_jasa'],
                            'tarif_extra_premi' => $polis1['tarif_extra_premi'],
                            'jumlah_extra_premi' => $polis1['jumlah_extra_premi'],
                            '“status_akseptasi”' => '3',
                            'keterangan' => 'Dokumen Adaul dan F5 belum diterima'
                        ));
                    }

                echo json_encode($return, JSON_UNESCAPED_SLASHES);
                die;

            } else{

                $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error lainnya"));
                echo json_encode($return);
                die;

            }

        } else{

            if (!empty($polis)) {
                    if($med == 'CAC'){
                    $return = array("Result"=>array(
                        'status' => '200',
                        'kode_response' => '00',
                        'message' => 'Berhasil kirim pengajuan polis asuransi',
                        'nama' => $polis1['nama'],
                        'nomor_rekening' => $polis1['nomor_rekening'],
                        'nomor_pk' => $polis1['nomor_pk'],
                        'jenis_pengajuan' => '1',
                        'asuransi' => $polis1['asuransi'],
                        'jenis_penjaminan' => $polis1['jenis_penjaminan'],
                        'no_polis' => $polis1['no_polis'],
                        's&k' => $sk,
                        'periode_awal' => $awal,
                        'periode_akhir' => $akhir,
                        'nilai_penjaminan' => $polis1['nilai_penjaminan'],
                        'tarif_imbal_jasa' => $polis1['tarif_imbal_jasa'],
                        'jumlah_imbal_jasa' => $polis1['jumlah_imbal_jasa'],
                        'tarif_extra_premi' => $polis1['tarif_extra_premi'],
                        'jumlah_extra_premi' => $polis1['jumlah_extra_premi'],
                        '“status_akseptasi”' => '5',
                        'keterangan' => '-'
                    ));
                    }else{
                        $return = array("Result"=>array(
                            'status' => '200',
                            'kode_response' => '00',
                            'message' => 'Berhasil kirim pengajuan polis asuransi',
                            'nama' => $polis1['nama'],
                            'nomor_rekening' => $polis1['nomor_rekening'],
                            'nomor_pk' => $polis1['nomor_pk'],
                            'jenis_pengajuan' => '2',
                            'asuransi' => $polis1['asuransi'],
                            'jenis_penjaminan' => $polis1['jenis_penjaminan'],
                            'no_polis' => $polis1['no_polis'],
                            's&k' => $sk,
                            'periode_awal' => $awal,
                            'periode_akhir' => $akhir,
                            'nilai_penjaminan' => $polis1['nilai_penjaminan'],
                            'tarif_imbal_jasa' => $polis1['tarif_imbal_jasa'],
                            'jumlah_imbal_jasa' => $polis1['jumlah_imbal_jasa'],
                            'tarif_extra_premi' => $polis1['tarif_extra_premi'],
                            'jumlah_extra_premi' => $polis1['jumlah_extra_premi'],
                            '“status_akseptasi”' => '3',
                            'keterangan' => 'Dokumen Adaul dan F5 belum diterima'
                        ));
                    }

                    echo json_encode($return, JSON_UNESCAPED_SLASHES);
                    die;

                    } else{

                        $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error lainnya"));
                        echo json_encode($return);
                        die;

                    }

                }
                       
                }else {
                    $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Error lainnya"));
                    echo json_encode($return);
                    die;
                }
    }
























    //     $kodeh2h = $data_json['kodeh2h'];
    //     $username = $data_json['username'];
    //     $password = $data_json['password'];

      
    //     if ($kodeh2h == '' || $username == '' || $password == '') {
    //         //$connection = $this->db->get('cek_komunikasi')->result();
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }     
    //     else if ($kodeh2h != '633e18b8cf68b8e2d1d5fbc6ab30deaf06b0e' || $username != 'brk' || $password != 'brk!')
    //     {
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }    
    //     else {
    //         date_default_timezone_set('Asia/Jakarta');
    //         $stcurl = '0';
    //         // foreach($data_json as $idx => $dt){
    //             $arr_insert = array(
    //                 'kodeh2h' => $data_json['kodeh2h'],
    //                 'username' => $data_json['username'],
    //                 'password' => $data_json['password'],
    //                 'cab' => $data_json['cab'],
    //                 'pk' => $data_json['pk'],
    //                 'norek' => $data_json['norek'],
    //                 'nama' => $data_json['nama'],
    //                 'lahir' => $data_json['lahir'],
    //                 'buka' => $data_json['buka'],
    //                 'tempo' => $data_json['tempo'],
    //                 'plankredit' => $data_json['plan'],
    //                 'amount' => $data_json['amount'],
    //                 'id' => $data_json['id'],
    //                 'ktp' => $data_json['ktp'],
    //                 'rate' => $data_json['rate'],
    //                 'sex' => $data_json['sex'],
    //                 'rate_asuransi' => $data_json['rate_asuransi'],
    //                 'asuransi' => $data_json['asuransi'],
    //                 'npwp' => $data_json['npwp'],
    //                 'old_pk' => $data_json['old_pk'],
    //                 'date_created' => date("Y-m-d H:i:s"),
    //                 'date_modified' => date("Y-m-d H:i:s"),
    //                 'stcurl' => $stcurl,
    //             );
    //             // var_dump($arr_insert);
    //             // die();
    //             $insert = $this->db->query("insert into Tampung_Riau.dbo.DataRiau (
    //             kodeh2h,
    //             userid,
    //             password,
    //             cab,
    //             pk,
    //             norek,
    //             nama,
    //             lahir,
    //             buka,
    //             tempo,
    //             plankredit,
    //             amount,
    //             id,
    //             ktp,
    //             rate,
    //             sex,
    //             rate_asuransi,
    //             asuransi,
    //             npwp,
    //             old_pk,
    //             date_created,
    //             date_modified,
    //             stcurl
    //             ) 
    //             values (
    //             '".$arr_insert['kodeh2h']."',
    //             '".$arr_insert['username']."',
    //             '".$arr_insert['password']."',
    //             '".$arr_insert['cab']."',
    //             '".$arr_insert['pk']."',
    //             '".$arr_insert['norek']."',
    //             '".$arr_insert['nama']."',
    //             '".$arr_insert['lahir']."',
    //             '".$arr_insert['buka']."',
    //             '".$arr_insert['tempo']."',
    //             '".$arr_insert['plankredit']."',
    //             '".$arr_insert['amount']."',
    //             '".$arr_insert['id']."',
    //             '".$arr_insert['ktp']."',
    //             '".$arr_insert['rate']."',
    //             '".$arr_insert['sex']."',
    //             '".$arr_insert['rate_asuransi']."',
    //             '".$arr_insert['asuransi']."',
    //             '".$arr_insert['npwp']."',
    //             '".$arr_insert['old_pk']."',
    //             '".$arr_insert['date_created']."',
    //             '".$arr_insert['date_modified']."',
    //             '".$arr_insert['stcurl']."'
    //             )");
    //             // $insert = $this->db->insert('DataRiau', $arr_insert);

    //             if($insert === false) $success = false;
    //         }
    //     }

    //     if($success){

            

    //         // $ch = curl_init();
    //             // $data_string = json_encode($data_json);
    //         //     curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
    //         //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    //         //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    //         //     // curl_setopt($ch, CURLOPT_PORT, 888);
    //         //     curl_setopt($ch, CURLOPT_POST, true);
    //         //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //         //     // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //         //     // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //         //     curl_exec($ch);
    //         //     curl_close($ch);

    //             // if(curl_errno($ch)){
    //             //     $err = curl_error($ch);
    //             //     echo $err;
    //             // }


    //         $return = array("Result"=>array('status' => '200', "message"=>"Data sudah di input"));

    //         echo json_encode($return);
    //         die;
    //     }else{
    //         $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
    //         echo json_encode($return);
    //         die;
    //     }
    // }

    public function kirimdata(){

        $sql = $this->db->query("select * from Tampung_Riau.dbo.DataRiau where stcurl = 0 ")->result_array();

        // var_dump($sql);
        // die();

                if (!empty($sql)) {
                        $asuransi = '';

                        foreach ($sql as $key => $value) {

                        $norek = $value['norek'];
                        $arr_insert = array(
                        'id_transaksi' => $value['id_transaksi'],
                        'kode_broker' => $value['kode_broker'],
                        'cab' => $value['cab'],
                        'pk' => $value['pk'],
                        'norek' => $value['norek'],
                        'nama' => $value['nama'],
                        'lahir' => $value['lahir'],
                        'buka' => $value['buka'],
                        'tempo' => $value['tempo'],
                        'plan' => $value['plankredit'],
                        'amount' => $value['amount'],
                        'id' => $value['id'],
                        'ktp' => $value['ktp'],
                        'rate' => $value['rate'],
                        'sex' => $value['sex'],
                        'rate_asuransi' => $value['rate'],
                        'asuransi' => $asuransi,
                        'npwp' => $value['npwp'],
                        'old_pk' => $value['old_pk'],
                        'benefit' => $value['benefit'],

                        );

                        $ch = curl_init();
                        $data_string = json_encode($arr_insert);
                        // curl_setopt($ch, CURLOPT_HEADER, false);
                        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/riaupan/Callbackapi");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        // curl_setopt($ch, CURLOPT_PORT, 888);
                        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                        // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        // if(curl_errno($ch)){
                        //     $err = curl_error($ch);
                        //     echo $err;
                        // }
                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            echo $httpcode;
                            if($httpcode == '200'){
                                $json = json_decode($result, true);

                             if($json['status'] == '200'){
                                     $this->db->query("update Tampung_Riau.dbo.DataRiau set stcurl = '1' where norek = '$norek'");
                             }
                             }
                            //  else{
        
                            // }
                         }
                    
                }
    }

}
