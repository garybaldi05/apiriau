<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('Login_m');
    }

    public function index()
    {
      if($this->Login_m->logged_id())
      {
        redirect('beranda');
      }else{
          $this->form_validation->set_rules('username', 'Username', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');

          $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
              <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
          //cek validasi
          if ($this->form_validation->run() == TRUE) {
          //get data dari FORM
          $username = $this->input->post("username", TRUE);
          $password = MD5($this->input->post('password', TRUE));

          //checking data via model
          $checking = $this->Login_m->check_login('tm_user', array('username' => $username), array('password' => $password));

          //jika ditemukan, maka create session
          if ($checking != FALSE) {
              foreach ($checking as $apps) {

                  $session_data = array(
                      'id_user'           => $apps->id_user,
                      'nik'               => $apps->nik,
                      'username'          => $apps->username,
                      'id_jabatan'        => $apps->id_jabatan,
                      'id_kantor'         => $apps->id_kantor,
                      'nama'              => $apps->nama,
                      'status'            => $apps->status,
                      'image'             => $apps->image,
                      'image_ttd'         => $apps->image_ttd
                  );
                  //set session userdata
                  $this->session->set_userdata($session_data);

                if ($session_data['status'] == '1') {
                  redirect('beranda');
                }
                else {
                  redirect('login');
                }
              }
          }else{

              $data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
                  <div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> username atau password salah!</div></div>';
              $this->load->view('login', $data);
          }
      }else{
          $this->load->view('login');
      }

    }

  }

  public function logout() 
  {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('id_group');
    $this->session->unset_userdata('password');
    $this->session->unset_userdata('image');
    $this->session->unset_userdata('image_ttd');
    $this->session->unset_userdata('nama');
    session_destroy();
    redirect('login');
  }
}