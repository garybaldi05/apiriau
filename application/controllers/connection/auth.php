<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->cekkoneksi();
    }

    // nantinya server bank akan manggil function ini dengan nembak url :
    // IPServer/namaaplikasi/callbackapi/paid
    // contoh: http://203.210.84.220/aplikasiPAN/callbackapi/getdatariau

    public function cekkoneksi(){
        // $data = file_get_contents('php://input'); //data yang dikirim dari bank berbentuk json
        $data = $this->input->raw_input_stream;
        // var_dump($data);
        // die();
        $data_json = json_decode($data, true); //parse json menjadi array

        //selanjutnya lakukan proses data sesuai sesuai kebutuhan
        $success = true;
        if(!empty($data_json)){
        $kodeh2h = '633e18b8cf68b8e2d1d5fbc6ab30deaf06b0e';
        $userid = 'brk';
        $password = 'brk!';

        foreach($data_json as $idx => $dt){
                $arr_insert = array(
                    'kodeh2h' => $dt['kodeh2h'],
                    'userid' => $dt['userid'],
                    'password' => $dt['password']
                );

                if($arr_insert['kodeh2h'] != $kodeh2h || $arr_insert['userid'] != $userid || $arr_insert['password'] != $password) $success = false;
            }
        }
        

        if($success){
            $return = array("Result"=>array('status' => '200', "message"=>"Koneksi Ok"));
            echo json_encode($return);
            die;
        }else{
            $return = array("Result"=>array('status' => '404', "message"=>"You dont have permission to access this service"));
            echo json_encode($return);
            die;
        }
    }

}
