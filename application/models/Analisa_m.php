<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisa_m extends CI_Model
{

	function fak_no()
	{
		$kodena	= $this->session->userdata('id_kantor');
		$this->db->select('RIGHT(tt_fak.fak_no,2) as no_order', FALSE);
		$this->db->order_by('fak_id','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tt_fak');       
		if($query->num_rows() <> 0){      
		$data = $query->row();      
		$kode = intval($data->no_order) + 1;    
		}
		else {      
		$kode = 1;    
		}
		$kodemax = str_pad($kode++, 3, "0", STR_PAD_LEFT); 
		date_default_timezone_set('Asia/Jakarta');
		$kodejadi = "FAK0".date('s').$kodena.'-'.date('ym-'). $kodemax;   
		return $kodejadi;  
		
	}

	function list_analisa($kantor)
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.plafon_disetujui,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama 
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			where a.id_kantor = $kantor or a.id_perusahaan = $kantor
			order by a.fak_id desc
			");
		return $query->result_array();
	}

	function list_analisa_r($kantor)
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama,
			d.nama as nama_ks,
			e.nama as nama_pic_input_data,
			f.nama as nama_spv,
			g.nama as nama_pincab,
			h.nama as nama_dir,
			i.nama as nama_pic_j_akad,
			j.nama as nama_ops
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			left join tm_user d on b.app_id_1 = d.id_user 
			left join tm_user e on b.app_id_2 = e.id_user 
			left join tm_user f on b.app_id_3 = f.id_user 
			left join tm_user g on b.app_id_4 = g.id_user 
			left join tm_user h on b.app_id_5 = h.id_user 
			left join tm_user i on b.app_id_6 = i.id_user 
			left join tm_user j on b.app_id_7 = j.id_user 
			where a.id_kantor = $kantor or a.id_perusahaan = $kantor 
			and month(a.update_date) = month(current_date())
			");
		return $query->row_array();
	}

	function list_analisa_all() //pt resa
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.plafon_disetujui,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama 
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			order by a.fak_id desc
			");
		return $query->result_array();
	}

	function list_analisa_r_all() //pt resa
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama,
			d.nama as nama_ks,
			e.nama as nama_pic_input_data,
			f.nama as nama_spv,
			g.nama as nama_pincab,
			h.nama as nama_dir,
			i.nama as nama_pic_j_akad,
			j.nama as nama_ops
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			left join tm_user d on b.app_id_1 = d.id_user 
			left join tm_user e on b.app_id_2 = e.id_user 
			left join tm_user f on b.app_id_3 = f.id_user 
			left join tm_user g on b.app_id_4 = g.id_user 
			left join tm_user h on b.app_id_5 = h.id_user 
			left join tm_user i on b.app_id_6 = i.id_user 
			left join tm_user j on b.app_id_7 = j.id_user 
			");
		return $query->row_array();
	}

	function list_analisa_search($dari,$sampai,$kantor)
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.plafon_disetujui,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama 
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			where a.id_kantor = $kantor or a.id_perusahaan = $kantor 
			and a.tanggal_permohonan between '".date('Y-m-d', strtotime($dari))."' and '".date('Y-m-d', strtotime($sampai))."'
			order by a.fak_id desc
			");
		return $query->result_array();
	}

	function list_analisa_r_search($kantor,$dari,$sampai)
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama,
			d.nama as nama_ks,
			e.nama as nama_pic_input_data,
			f.nama as nama_spv,
			g.nama as nama_pincab,
			h.nama as nama_dir,
			i.nama as nama_pic_j_akad,
			j.nama as nama_ops
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			left join tm_user d on b.app_id_1 = d.id_user 
			left join tm_user e on b.app_id_2 = e.id_user 
			left join tm_user f on b.app_id_3 = f.id_user 
			left join tm_user g on b.app_id_4 = g.id_user 
			left join tm_user h on b.app_id_5 = h.id_user 
			left join tm_user i on b.app_id_6 = i.id_user 
			left join tm_user j on b.app_id_7 = j.id_user 
			where a.id_kantor = $kantor or a.id_perusahaan = $kantor 
			and a.tanggal_permohonan between '".date('Y-m-d', strtotime($dari))."' and '".date('Y-m-d', strtotime($sampai))."'
			");
		return $query->row_array();
	}

	function list_analisa_r_all_search($dari,$sampai) //pt resa
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama,
			d.nama as nama_ks,
			e.nama as nama_pic_input_data,
			f.nama as nama_spv,
			g.nama as nama_pincab,
			h.nama as nama_dir,
			i.nama as nama_pic_j_akad,
			j.nama as nama_ops
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			left join tm_user d on b.app_id_1 = d.id_user 
			left join tm_user e on b.app_id_2 = e.id_user 
			left join tm_user f on b.app_id_3 = f.id_user 
			left join tm_user g on b.app_id_4 = g.id_user 
			left join tm_user h on b.app_id_5 = h.id_user 
			left join tm_user i on b.app_id_6 = i.id_user 
			left join tm_user j on b.app_id_7 = j.id_user 
			where a.tanggal_permohonan between '".date('Y-m-d', strtotime($dari))."' and '".date('Y-m-d', strtotime($sampai))."'
			");
		return $query->row_array();
	}

	function list_analisa_search_all($dari,$sampai)
	{
		$query = $this->db->query("SELECT
			a.fak_id,
			a.fak_no,
			a.nama_lengkap,
			a.no_ktp,
			a.jumlah_kredit_diajukan,
			a.jangka_waktu,
			a.tanggal_permohonan,
			a.status,
			a.update_by,
			a.update_date,
			b.app_status_1,
			b.app_keterangan_1,
			b.app_tanggal_1,
			b.app_status_2,
			b.app_keterangan_2,
			b.app_tanggal_2,
			b.app_status_3,
			b.app_keterangan_3,
			b.app_tanggal_3,
			b.app_status_4,
			b.app_keterangan_4,
			b.app_tanggal_4,
			b.app_status_5,
			b.app_keterangan_5,
			b.app_tanggal_5,
			b.app_status_6,
			b.app_keterangan_6,
			b.app_tanggal_6,
			b.app_status_7,
			b.app_keterangan_7,
			b.app_tanggal_7,
			c.nama 
		FROM
			tt_fak AS a
			left JOIN tt_app AS b ON a.fak_id = b.fak_id
			left JOIN tm_user AS c ON a.update_by = c.id_user
			where a.tanggal_permohonan between '".date('Y-m-d', strtotime($dari))."' and '".date('Y-m-d', strtotime($sampai))."'
			order by a.fak_id desc
			");
		return $query->result_array();
	}

	

	function fak($fak_id)
	{
		$query = $this->db->query("select * from tt_fak where fak_id = $fak_id");
		return $query->row();
	}

	function arus_kas($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.status, b.* from tt_fak a left join tt_arus_kas b on a.fak_id = b.fak_id where a.fak_id = $fak_id");
		return $query->row();
	}

	function asset($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, b.* from tt_fak a left join tt_asset b on a.fak_id = b.fak_id where a.fak_id = $fak_id");
		return $query->row();
	}

	function pinjaman($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, b.* from tt_fak a left join tt_pinjaman b on a.fak_id = b.fak_id where a.fak_id = $fak_id");
		return $query->row();
	}

	function dokumen($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a 
				left join tt_dokumen b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function mpk($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, a.jumlah_kredit_diajukan, a.jangka_waktu as jangka_waktos, b.*, c.*, d.sisa_penghasilan from tt_fak a  
				left join tt_mpk b on a.fak_id = b.fak_id
				left join tt_app c on a.fak_id = c.fak_id
				left join tt_arus_kas d on a.fak_id = d.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function mpkna_1($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 1 ");
		return $query->row_array();
	}

	function mpkna_2($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 2 ");
		return $query->row_array();
	}

	function mpkna_3($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 3 ");
		return $query->row_array();
	}

	function mpkna_4($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 4 ");
		return $query->row_array();
	}

	function mpkna_5($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 5 ");
		return $query->row_array();
	}

	function mpkna_6($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 6 ");
		return $query->row_array();
	}

	function mpkna_7($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 7 ");
		return $query->row_array();
	}

	function app_mpk($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a  
				left join tt_app b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id
				group by fakna");
		return $query->row();
	}

	function akad($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.fak_no, a.nama_lengkap, a.status, b.*, c.nama_kantor, c.cabang from tt_fak a  
				left join tt_akad b on a.fak_id = b.fak_id
				left join tm_kantor c on b.bank = c.id_kantor
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function dokumen_pic($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_perusahaan = $kantor and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_pic($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_perusahaan = $kantor and 
					a.status >= 7 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_pending_pic($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_perusahaan = $kantor and 
					a.status = 6 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function tutup_pic($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					join tt_app b on a.fak_id = b.fak_id
					where a.id_perusahaan = $kantor and 
					b.app_status_1 = 5 or b.app_status_2 = 5 or
					b.app_status_3 = 5 or b.app_status_4 = 5 or
					b.app_status_5 = 5 or b.app_status_6 = 5 or 
					b.app_status_7 = 5 and month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function dokumen_ks($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_kantor = $kantor and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_ks($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_kantor = $kantor and 
					a.status >= 7 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_pending_ks($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.id_kantor = $kantor and 
					a.status = 6 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function tutup_ks($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					join tt_app b on a.fak_id = b.fak_id
					where a.id_kantor = $kantor and 
					b.app_status_1 = 5 or b.app_status_2 = 5 or
					b.app_status_3 = 5 or b.app_status_4 = 5 or
					b.app_status_5 = 5 or b.app_status_6 = 5 or 
					b.app_status_7 = 5 and month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	} 

	function dokumen_resa($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_resa($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.status >= 7 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function akad_pending_resa($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					where a.status = 6 and 
					month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function tutup_resa($kantor)
	{
		$query = $this->db->query("select count(a.fak_id) as jumlahna from tt_fak a 
					join tt_app b on a.fak_id = b.fak_id
					where b.app_status_1 = 5 or b.app_status_2 = 5 or
					b.app_status_3 = 5 or b.app_status_4 = 5 or
					b.app_status_5 = 5 or b.app_status_6 = 5 or 
					b.app_status_7 = 5 and month(a.tanggal_permohonan) = month(current_date())");
		return $query->row();
	}

	function auto_debet_coll()
	{
		$query = $this->db->query("select a.fak_no, a.fak_id, a.nama_lengkap, a.no_ktp, b.angsuran, c.status
				from tt_fak a 
                join tt_mpk b on a.fak_id = b.fak_id
                left join tt_auto_debet c on a.fak_no = c.fak_no
                group by a.fak_id order by a.fak_id desc");
		return $query->result_array();
	}

	function auto_debet($kantor)
	{
		$query = $this->db->query("select a.fak_no, a.fak_id, a.nama_lengkap, a.no_ktp, b.angsuran, c.status
				from tt_fak a 
                join tt_mpk b on a.fak_id = b.fak_id
                join tt_akad d on a.fak_id = d.fak_id
                left join tt_auto_debet c on a.fak_no = c.fak_no
				where d.bank = $kantor
                group by a.fak_id order by a.fak_id desc");
		return $query->result_array();
	}

	function auto_debet_r($kantor)
	{
		$query = $this->db->query("select a.fak_no, a.fak_id, a.nama_lengkap, a.no_ktp, b.angsuran, c.status
				from tt_fak a 
                join tt_mpk b on a.fak_id = b.fak_id
                join tt_akad d on a.fak_id = d.fak_id
                left join tt_auto_debet c on a.fak_no = c.fak_no
				where d.bank = $kantor
                group by a.fak_id");
		return $query->result();
	}

	function print_fak($fak_id)
	{
		$query = $this->db->query("select a.*, 
				b.jenis_permohonan as mohon,
				c.jenis_jaminan as jaminanna,
				d.status_rumah,
				e.pendidikan as pendidikanna,
				f.status_kawin as status_kawinna,
				g.hubungan,
				h.jenis_pekerjaan as gawena,
				i.jenis_pekerjaan as gaweistri,
				j.jenis_aset_1,
				j.nilai_taksasi_1,
				j.keterangan_1,
				j.jenis_aset_2,
				j.nilai_taksasi_2,
				j.keterangan_2,
				j.jenis_aset_3,
				j.nilai_taksasi_3,
				j.keterangan_3,
				j.jenis_aset_4,
				j.nilai_taksasi_4,
				j.keterangan_4,
				j.jenis_aset_5,
				j.nilai_taksasi_5,
				j.keterangan_5,
				k.nama_lembaga_keuangan_1,
				k.plafond_1,
				k.baki_debet_1,
				k.angsuran_1,
				k.keterangan_pinjaman_1,
				k.nama_lembaga_keuangan_2,
				k.plafond_2,
				k.baki_debet_2,
				k.angsuran_2,
				k.keterangan_pinjaman_2,
				k.nama_lembaga_keuangan_3,
				k.plafond_3,
				k.baki_debet_3,
				k.angsuran_3,
				k.keterangan_pinjaman_3,
				k.nama_lembaga_keuangan_4,
				k.plafond_4,
				k.baki_debet_4,
				k.angsuran_4,
				k.keterangan_pinjaman_4,
				k.nama_lembaga_keuangan_5,
				k.plafond_5,
				k.baki_debet_5,
				k.angsuran_5,
				k.keterangan_pinjaman_5,
				l.gaji_pokok,
				l.uang_makan_transport,
				l.tunjangan,
				l.nama_tunjangan_1,
				l.nama_tunjangan_2,
				l.nama_tunjangan_3,
				l.nama_tunjangan_4,
				l.nama_tunjangan_5,
				l.tunjangan_1,
				l.tunjangan_2,
				l.tunjangan_3,
				l.tunjangan_4,
				l.tunjangan_5,
				l.total_penghasilan,
				l.jht,
				l.kesehatan,
				l.pph,
				l.potongan,
				l.nama_potongan_1,
				l.nama_potongan_2,
				l.nama_potongan_3,
				l.nama_potongan_4,
				l.potongan_1,
				l.potongan_2,
				l.potongan_3,
				l.potongan_4,
				l.total_pengeluaran,
				l.sisa_penghasilan

				from tt_fak a
				join tm_jenis_permohonan b on a.jenis_permohonan = b.id_jenis_permohonan
				join tm_jenis_jaminan c on a.jenis_jaminan = c.id_jenis_jaminan
				join tm_status_rumah d on a.status_kepemilikan_rumah = d.id_status_rumah
				join tm_pendidikan e on a.pendidikan = e.id_pendidikan
				join tm_status_kawin f on a.status_kawin = f.id_status_kawin
				join tm_hubungan g on a.hubungan_referensi = g.id_hubungan
				join tm_jenis_pekerjaan h on a.jenis_pekerjaan = h.id_jenis_pekerjaan
				join tm_jenis_pekerjaan i on a.jenis_pekerjaan_suami_istri = i.id_jenis_pekerjaan
				join tt_asset j on a.fak_id = j.fak_id
				join tt_pinjaman k on a.fak_id = k.fak_id
				join tt_arus_kas l on a.fak_id = l.fak_id
				 ");
		return $query->row();
	}

	function kantor_pic()
	{
		$query = $this->db->query("select a.*, b.status as statusna 
			from tm_kantor a 
			join tm_status b on a.status = b.id_status 
			where a.kantor_kategori = 4 and a.status = 1");
		return $query->result_array();
	}

	function fee_na($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, b.* from tt_fak a left join tm_fee b on a.fak_id = b.fak_id where a.fak_id = $fak_id");
		return $query->row();
	}

	function list_fee_plafond()
	{
		$query = $this->db->query("select a.fak_id as fakna, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, b.create_date, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' group by a.id_perusahaan");
		return $query->result_array();
	}

	function fee_plafond_hrd($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, d.tanggal, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, c.fee_plafond_hrd, b.create_date, d.status, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id left join tt_fee_plafond_hrd d on c.fak_id = d.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' and a.id_perusahaan = $id_perusahaan group by a.id_perusahaan");
		return $query->result_array();
	}

	function fee_plafond_resa($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, d.tanggal, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, c.fee_plafond_resa, b.create_date, d.status, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id left join tt_fee_plafond_resa d on c.fak_id = d.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' and a.id_perusahaan = $id_perusahaan group by a.id_perusahaan");
		return $query->result_array();
	}

	function list_fee_produk()
	{
		$query = $this->db->query("select a.fak_id as fakna, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, b.create_date, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' group by a.id_perusahaan");
		return $query->result_array();
	}

	function fee_produk_hrd($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, d.tanggal, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, c.fee_produk_hrd, b.create_date, d.status, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id left join tt_fee_produk_hrd d on c.fak_id = d.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' and a.id_perusahaan = $id_perusahaan group by a.id_perusahaan");
		return $query->result_array();
	}

	function fee_produk_resa($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, d.tanggal, a.id_perusahaan, a.nama_lengkap, a.fak_no, a.no_ktp, a.plafon_disetujui, c.fee_produk_resa, b.create_date, d.status, e.nama_kantor from tt_fak a join tt_serahterima b on a.fak_id = b.fak_id join tm_fee c on a.fak_id = c.fak_id left join tt_fee_produk_resa d on c.fak_id = d.fak_id join tm_kantor e on a.id_perusahaan = e.id_kantor where b.create_date != '' and a.id_perusahaan = $id_perusahaan group by a.id_perusahaan");
		return $query->result_array();
	}

	function list_fee_angsuran()
	{
		$query = $this->db->query("select a.fak_id as fakna, a.id_perusahaan, a.fak_no, a.nama_lengkap, a.no_ktp, f.nama_kantor, a.plafon_disetujui, c.angsuran, b.fee_angsuran_hrd
			from tt_fak a 
			join tm_fee b on a.fak_id = b.fak_id
			join tt_mpk c on a.fak_id = c.fak_id
			join tm_kantor f on a.id_perusahaan = f.id_kantor
			left join tt_fee_angsuran_hrd d on b.fak_id = d.faK_id
			left join tt_auto_debet e on a.fak_no = e.fak_no
			where e.status = 1 
			group by a.id_perusahaan
			");
		return $query->result_array();
	}

	function fee_angsuran_hrd($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.id_perusahaan, a.fak_no, a.nama_lengkap, d.tanggal, d.status, e.periode, a.no_ktp, f.nama_kantor, a.plafon_disetujui, c.angsuran, b.fee_angsuran_hrd
			from tt_fak a 
			join tm_fee b on a.fak_id = b.fak_id
			join tt_mpk c on a.fak_id = c.fak_id
			join tm_kantor f on a.id_perusahaan = f.id_kantor
			left join tt_fee_angsuran_hrd d on b.fak_id = d.faK_id
			left join tt_auto_debet e on a.fak_no = e.fak_no
			where e.status = 1 and a.id_perusahaan = $id_perusahaan
			group by a.id_perusahaan
			order by c.no_urut desc");
		return $query->result_array();
	}

	function fee_angsuran_resa($id_perusahaan)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.id_perusahaan, a.fak_no, a.nama_lengkap, d.tanggal, d.status, e.periode, a.no_ktp, f.nama_kantor, a.plafon_disetujui, c.angsuran, b.fee_angsuran_resa
			from tt_fak a 
			join tm_fee b on a.fak_id = b.fak_id
			join tt_mpk c on a.fak_id = c.fak_id
			join tm_kantor f on a.id_perusahaan = f.id_kantor
			left join tt_fee_angsuran_resa d on b.fak_id = d.faK_id
			left join tt_auto_debet e on a.fak_no = e.fak_no
			where e.status = 1 and a.id_perusahaan = $id_perusahaan
			group by a.id_perusahaan
			order by c.no_urut desc");
		return $query->result_array();
	}

	function filter_kantor_pic($kantor)
	{
		$query= $this->db->query("select * from tm_kantor where id_kantor = $kantor");
		return $query->result_array();
	}

	function filter_kantor_collection()
	{
		$query= $this->db->query("select * from tm_kantor where kantor_kategori = 4");
		return $query->result_array();
	}

	function serahterima($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.fak_no, a.status, b.* from tt_fak a left join tt_serahterima b on a.fak_id = b.fak_id");
		return $query->row();
	}

	function cek_ktp($no_ktp) 
	{
		$this->db->where('no_ktp',$no_ktp);
		$query = $this->db->get('tt_fak');
		if($query->num_rows()>0)
		{
		return 1;	
		}
		else
		{
		return 0;	
		}
    }

    function limpahan()
    {
    	$query = $this->db->query("select * from tt_limpahan");
    	return $query->result_array();
    }

	function limpahan_nik($ktp)
    {
    	$query = $this->db->query("select * from tt_limpahan where ktp = $ktp");
    	return $query->row();
    }    

    function limpahan_data($ktp)
    {
    	$query = $this->db->query("select a.ktp as ktpna, b.* from tt_limpahan a left join tt_limpahan_dok b on a.ktp = b.ktp where a.ktp = $ktp");
    	return $query->row();
    }

    function limpahan_dokumen()
    {
    	$query = $this->db->query("select * from tm_kelengkapan_dokumen where id_dokumen in(1,3,6,7)");
    	return $query->result_array();
    }

    function akp($nik)
    {
    	$query = $this->db->query("select a.nik as nikna, a.nama, b.* from tt_limpahan a left join tt_mcm b on a.nik = b.nik where a.nik = $nik");
    	return $query->row();
    }

    function dokumen_tahan($fak_id)
    {
    	$query = $this->db->query("select a.faK_id as fakna, b.* from tt_fak a left join tt_dokumen_tahan b on a.fak_id = b.fak_id");
    	return $query->row();
    }

    function dokumenna()
    {
    	$query = $this->db->query("select * from tm_kelengkapan_dokumen");
    	return $query->result_array();
    }

    function mcm_print($nik)
    {
    	$query = $this->db->query("select a.* from tt_mcm a where nik = $nik");
    	return $query->row();
    }

    function angsuran($ktp)
    {
    	$query = $this->db->query("select count(c.id) as tunggakan, a.ktp, a.nama, a.periode, a.angsuran as angsuranna, a.plafond, b.angsuran 
			from tt_limpahan a 
			left join tt_limpahan_angsuran b on a.ktp = b.ktp 
			left join tt_limpahan_tunggakan c on a.ktp = c.ktp
			where a.ktp = $ktp");
    	return $query->result_array();
    }

    function form_pengajuan_pendamping($no_ktp)
    {
    	$query = $this->db->query("select * from tt_fak where no_ktp = $no_ktp");
    	return $query->row();
    }

}