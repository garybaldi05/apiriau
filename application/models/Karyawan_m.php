<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_m extends CI_Model
{
	function karyawan()
	{
		$query = $this->db->query("select a.*, b.*, c.*, d.id_status as statusna from tm_karyawan a 
			join tm_jabatan b on a.id_jabatan = b.id_jabatan 
			join tm_kantor c on a.id_kantor = c.id_kantor 
			join tm_status d on a.status = d.id_status 
			join tm_kantor_kategori e on c.kantor_kategori = e.id_kategori_kantor
			where e.id_kategori_kantor = 3
			order by a.id_jabatan asc");
		return $query->result_array();
	}
	function karyawan_user()
	{
		$query = $this->db->query("select * from tm_karyawan");
		return $query->result_array();
	}

	function karyawan_bpr()
	{
		$query = $this->db->query("select a.*, b.*, c.*, d.id_status as statusna from tm_karyawan a 
			join tm_jabatan b on a.id_jabatan = b.id_jabatan 
			join tm_kantor c on a.id_kantor = c.id_kantor 
			join tm_status d on a.status = d.id_status 
			join tm_kantor_kategori e on c.kantor_kategori = e.id_kategori_kantor
			where e.id_kategori_kantor = 1
			order by a.id_jabatan asc");
		return $query->result_array();
	}

	function karyawan_pic()
	{
		$query = $this->db->query("select a.*, b.*, c.*, d.id_status as statusna from tm_karyawan a 
			join tm_jabatan b on a.id_jabatan = b.id_jabatan 
			join tm_kantor c on a.id_kantor = c.id_kantor 
			join tm_status d on a.status = d.id_status 
			join tm_kantor_kategori e on c.kantor_kategori = e.id_kategori_kantor
			where e.id_kategori_kantor = 4
			order by a.id_jabatan asc");
		return $query->result_array();
	}

	function karyawan_bank_umum()
	{
		$query = $this->db->query("select a.*, b.*, c.*, d.id_status as statusna from tm_karyawan a 
			join tm_jabatan b on a.id_jabatan = b.id_jabatan 
			join tm_kantor c on a.id_kantor = c.id_kantor 
			join tm_status d on a.status = d.id_status 
			join tm_kantor_kategori e on c.kantor_kategori = e.id_kategori_kantor
			where e.id_kategori_kantor = 2
			order by a.id_jabatan asc");
		return $query->result_array();
	}

	function karyawan_koordinator()
	{
		$query = $this->db->query("select a.*, b.*, c.*, d.id_status as statusna from tm_karyawan a 
			join tm_jabatan b on a.id_jabatan = b.id_jabatan 
			join tm_kantor c on a.id_kantor = c.id_kantor 
			join tm_status d on a.status = d.id_status 
			join tm_kantor_kategori e on c.kantor_kategori = e.id_kategori_kantor
			where e.id_kategori_kantor = 5
			order by a.id_jabatan asc");
		return $query->result_array();
	}

	function karyawan_edit($id_karyawan)
	{
		$query = $this->db->query("select a.*, a.alamat as al, d.username from tm_karyawan a join tm_jabatan b on a.id_jabatan = b.id_jabatan join tm_kantor c on a.id_kantor = c.id_kantor join tm_user d on a.id_karyawan = d.id_karyawan where a.id_karyawan = $id_karyawan");
		return $query->row();
	}

	function jabatan()
	{
		$query = $this->db->query("select a.*, b.status as statusna, c.kategori_kantor from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status order by c.id_kategori_kantor asc");
		return $query->result_array();
	}

	function jabatan_resa()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status where c.id_kategori_kantor = 3");
		return $query->result_array();
	}

	function jabatan_bpr()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status where c.id_kategori_kantor = 1");
		return $query->result_array();
	}

	function jabatan_pic()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status where c.id_kategori_kantor = 4");
		return $query->result_array();
	}

	function jabatan_bank_umum()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status where c.id_kategori_kantor = 2");
		return $query->result_array();
	}

	function jabatan_koordinator()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_jabatan a join tm_kantor_kategori c on a.id_kategori_kantor = c.id_kategori_kantor join tm_status b on a.status = b.id_status where c.id_kategori_kantor = 5");
		return $query->result_array();
	}

	function jabatan_edit($id_jabatan)
	{
		$query = $this->db->query("select * from tm_jabatan where id_jabatan = $id_jabatan");
		return $query->row();
	}

	function kantor()
	{
		$query = $this->db->query("select a.*, b.status as statusna, c.kategori_kantor from tm_kantor a join tm_status b on a.status = b.id_status join tm_kantor_kategori c on a.kantor_kategori = c.id_kategori_kantor");
		return $query->result_array();
	}

	function kantor_resa()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_kantor a join tm_status b on a.status = b.id_status where a.kantor_kategori = 3 and a.status = 1");
		return $query->result_array();
	}

	function kantor_bpr()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_kantor a join tm_status b on a.status = b.id_status where a.kantor_kategori = 1 and a.status = 1");
		return $query->result_array();
	}

	function kantor_pic()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_kantor a join tm_status b on a.status = b.id_status where a.kantor_kategori = 4 and a.status = 1");
		return $query->result_array();
	}

	function kantor_bank_umum()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_kantor a join tm_status b on a.status = b.id_status where a.kantor_kategori = 2 and a.status = 1");
		return $query->result_array();
	}

	function kantor_koordinator()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_kantor a join tm_status b on a.status = b.id_status where a.kantor_kategori = 5 and a.status = 1");
		return $query->result_array();
	}

	function kantor_edit($id_kantor)
	{
		$query = $this->db->query("select * from tm_kantor where id_kantor = $id_kantor");
		return $query->row();
	}

	function produk()
	{
		$query = $this->db->query("select a.*, b.status as statusna from tm_produk a join tm_status b on a.status = b.id_status");
		return $query->result_array();
	}

	function produk_edit()
	{
		$query = $this->db->query("select * from tm_produk");
		return $query->row();
	}

	function status()
	{
		$query = $this->db->query("select * from tm_status ");
		return $query->result_array();
	}

	function kategori_kantor()
	{
		$query = $this->db->query("select * from tm_kantor_kategori ");
		return $query->result_array();
	}

	function user()
	{
		$query = $this->db->query("select a.id_user, a.nik, a.nama, b.jabatan, c.nama_kantor, a.username, a.status from tm_user a join tm_jabatan b on a.id_jabatan = b.id_jabatan join tm_kantor c on a.id_kantor = c.id_kantor order by a.id_jabatan asc ");
		return $query->result_array();
	}

	function user_edit($id_user)
	{
		$query = $this->db->query("select a.*, b.id_karyawan as karyawanna from tm_user a join tm_karyawan b on a.nik = b.nik where a.id_user = $id_user");
		return $query->row();
	}

	function jangka_waktu()
	{
		$query = $this->db->query("select * from tm_jangka_waktu");
		return $query->result_array();
	}

	function jenis_kelamin()
	{
		$query = $this->db->query("select * from tm_jenis_kelamin");
		return $query->result_array();
	}

	function pendidikan()
	{
		$query = $this->db->query("select * from tm_pendidikan");
		return $query->result_array();
	}

	function hubungan()
	{
		$query = $this->db->query("select * from tm_hubungan");
		return $query->result_array();
	}

	function approval()
	{
		$query = $this->db->query("select a.*, b.kantor from tm_approval a join tm_kantor b on a.id_wil = b.id_kantor");
		return $query->result_array();
	}

	function rekening($kantor)
	{
		$query = $this->db->query("select a.*, c.nama_kantor, d.sisa_penghasilan,  b.poto_akad, e.jenis_kelamin as jkna, f.jenis_pekerjaan as pekerjaan, g.status_kawin as status_kawinna, h.no_rekening_pendamping as rekeningna, i.nama_kantor as bankna, i.cabang
			from tt_fak a 
			join tt_akad b on a.fak_id = b.fak_id 
			join tm_kantor c on a.id_perusahaan = c.id_kantor
			join tt_arus_kas d on a.fak_id = d.fak_id
			join tm_jenis_kelamin e on a.jenis_kelamin = id_jenis_kelamin
			join tm_jenis_pekerjaan f on a.jenis_pekerjaan = f.id_jenis_pekerjaan
			join tm_status_kawin g on a.status_kawin = g.id_status_kawin
			left join tm_rekening_pendamping h on a.fak_no = h.fak_no
			join tm_kantor i on b.bank = i.id_kantor
			where b.bank = $kantor and a.status >= 7");
		return $query->result_array();
	}

	function rekening_r($kantor,$dari,$sampai)
	{
		$query = $this->db->query("select a.*, c.nama_kantor, d.sisa_penghasilan, b.status_rekening_pendamping, e.jenis_kelamin as jkna, f.jenis_pekerjaan as pekerjaan, g.status_kawin as status_kawinna, h.no_rekening_pendamping as rekeningna, i.nama_kantor as bankna, i.cabang
			from tt_fak a 
			join tt_akad b on a.fak_id = b.fak_id 
			join tm_kantor c on a.id_perusahaan = c.id_kantor
			join tt_arus_kas d on a.fak_id = d.fak_id
			join tm_jenis_kelamin e on a.jenis_kelamin = id_jenis_kelamin
			join tm_jenis_pekerjaan f on a.jenis_pekerjaan = f.id_jenis_pekerjaan
			join tm_status_kawin g on a.status_kawin = g.id_status_kawin
			left join tm_rekening_pendamping h on a.fak_no = h.fak_no
			join tm_kantor i on b.bank = i.id_kantor
			where b.bank = $kantor and a.status >= 7 and b.tanggal_akad between '".date('Y-m-d', strtotime($dari))."' and '".date('Y-m-d', strtotime($sampai))."'");
		return $query->result();
	}

	function count_ks($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 1 and a.status_baca = 0");
		return $query->row();
	}

	function count_pic($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_perusahaan = $kantor
				and a.status in(2,7) and a.status_baca = 0");
		return $query->row();
	}

	function count_admin()
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id
				where a.status in(1,2,4,5,6,7,8) and status_baca = 0
				");
		return $query->row();
	}

	function count_collection()
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id
				where a.status = 10 and status_baca = 0
				");
		return $query->row();
	}

	function count_ops()
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id
				where a.status in(8) and status_baca = 0
				");
		return $query->row();
	}

	function count_spv($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 4 and a.status_baca = 0");
		return $query->row();
	}

	function count_pincab($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 5 and a.status_baca = 0");
		return $query->row();
	}

	function count_dir($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 6 and a.status_baca = 0");
		return $query->row();
	}

	function count_bank_umum($kantor)
	{
		$query = $this->db->query("select count(a.id_pemberitahuan)
				as jumlahna
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
                    join tt_akad c on a.fak_id = c.fak_id
				where a.status = 0 and
					b.id_kantor = $kantor");
		return $query->row();
	}

	function notif_ks($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 1 group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5");
		return $query->result_array();
	}

	function notif_pic($kantor) //NOTIFIKASI PIC
	{
		$query = $this->db->query("select a.*
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_perusahaan = $kantor
				and a.status in(2,7) group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5");
		return $query->result_array();
	}

	function notif_admin() //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where a.status in(1,2,4,5,6,7,8) group by a.id_pemberitahuan order by a.id_pemberitahuan desc 
				limit 5
				");
		return $query->result_array();
	}

	function notif_collection() 
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where a.status = 10 group by a.id_pemberitahuan order by a.id_pemberitahuan desc 
				limit 5
				");
		return $query->result_array();
	}

	function notif_ops() //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where a.status in(8) group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5
				");
		return $query->result_array();
	}

	function notif_spv($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 4  group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5");
		return $query->result_array();
	}

	function notif_pincab($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 5 group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5");
		return $query->result_array();
	}

	function notif_dir($kantor) //NOTIFIKASI direktur bpr
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
				where
					b.id_kantor = $kantor
				and a.status = 6 group by a.id_pemberitahuan order by a.id_pemberitahuan desc
				limit 5");
		return $query->result_array();
	}

	function notif_bank_umum($kantor)
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
                    join tt_akad c on b.fak_id = c.fak_id
				where a.status = 0");
		return $query->result_array();
	}

	function all_notif_ks($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where
					b.id_kantor = $kantor
				and a.status = 1 group by a.id_pemberitahuan
				order by a.tanggal desc");
		return $query->result_array();
	}

	function all_notif_admin() 
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where a.status in(1,2,4,5,6,7,8) group by a.id_pemberitahuan
				order by a.tanggal desc
				");
		return $query->result_array();
	}

	function all_notif_collection() 
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where a.status = 10 group by a.id_pemberitahuan
				order by a.tanggal desc
				");
		return $query->result_array();
	}

	function all_notif_ops() 
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where a.status in(8) group by a.id_pemberitahuan
				order by a.tanggal desc
				");
		return $query->result_array();
	}

	function all_notif_pic($kantor) //NOTIFIKASI PIC
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where
					b.id_perusahaan = $kantor
				and a.status in(2,7) group by a.id_pemberitahuan
				order by a.tanggal desc");
		return $query->result_array();
	}

	function all_notif_spv($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where
					b.id_kantor = $kantor
				and a.status = 4 group by a.id_pemberitahuan
				order by a.tanggal desc");
		return $query->result_array();
	}

	function all_notif_pincab($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where
					b.id_kantor = $kantor
				and a.status = 5 group by a.id_pemberitahuan
				order by a.tanggal desc");
		return $query->result_array();
	}

	function all_notif_dir($kantor) //NOTIFIKASI KREDIT SUPPORT
	{
		$query = $this->db->query("select
					a.*, b.nama_lengkap, b.fak_no, c.nama_kantor 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
					join tm_kantor c on b.id_kantor = c.id_kantor
				where
					b.id_kantor = $kantor
				and a.status = 6 
				order by a.tanggal desc");
		return $query->result_array();
	}

	function all_notif_bank_umum($kantor)
	{
		$query = $this->db->query("select
					a.* 
				from
					tm_pemberitahuan a
					join tt_fak b ON a.fak_id = b.fak_id 
                    join tt_akad c on a.fak_id = c.fak_id
				where a.status = 0 and
					b.id_kantor = $kantor");
		return $query->result_array();
	}

	function kantor_kategori() //filter report
	{
		$query = $this->db->query("select * from tm_kantor_kategori where status = 1");
		return $query->result_array();
	}

	function role_jabatan()
	{
		$query = $this->db->query("select * from tt_role_jabatan");
		return $query->result_array();
	}


	function checkuser($username) 
	{
		$this->db->where('username',$username);
		$query = $this->db->get('tm_user');
		if($query->num_rows()>0)
		{
		return 1;	
		}
		else
		{
		return 0;	
		}
    }

    
	
}