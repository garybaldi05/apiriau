<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_m extends CI_Model
{

    public function dokumen_permohonan()
    {
        $query = $this->db->query("select a.* from tm_kelengkapan_dokumen a where a.id_dokumen in(1,2,3,13,14,8,7)");
        return $query->result_array();
    }

    public function dokumen()
    {
        $query = $this->db->query("select a.* from tm_kelengkapan_dokumen a where status = 1 and a.id_dokumen not in(10) ");
        return $query->result_array();
    }

    function dokumen_list($fak_id)
    {
        $query = $this->db->query("select a.*, b.kelengkapan, c.nama_ibu
            from tt_dokumen a 
            join tm_kelengkapan_dokumen b on a.id_dokumen = b.id_dokumen 
            join tt_fak c on a.fak_id = c.fak_id
            where a.fak_id = $fak_id and b.id_dokumen in(1,3,5,11)");
        return $query->result_array();
    }

    function dokumen_list_row($fak_id)
    {
        $query = $this->db->query("select a.*, b.fak_no, b.nama_lengkap, b.no_ktp, b.jumlah_kredit_diajukan, b.jangka_waktu 
                from tt_dokumen a
                join tt_fak b on a.fak_id = b.fak_id
                where a.fak_id = $fak_id or a.id_dokumen = 10
                ");
        return $query->row();
    }

    public function dokumen_ladu()
    {
        $query = $this->db->query("select a.* from tm_kelengkapan_dokumen a where a.id_dokumen in(16,17)");
        return $query->result_array();
    }

    function dokumena($fak_id)
    {
        $query = $this->db->query("SELECT id_dokumen,GROUP_CONCAT(dokumen) as dokumena
            FROM tt_dokumen
            where fak_id = $fak_id
            GROUP BY id_dokumen; ");
        return $query->row();
    }

    public function produk()
    {
        $query = $this->db->query("select * from tm_produk where kat_produk = 1");
        return $query->result_array();
    }

    function verifikasi()
    {
        $query = $this->db->query("select * from tm_verifikasi");
        return $query->result_array();
    }

    function jenis_kelamin()
    {
        $query = $this->db->query("select * from tm_jenis_kelamin");
        return $query->result_array();
    }

    function jenis_pekerjaan()
    {
        $query = $this->db->query("select * from tm_jenis_pekerjaan");
        return $query->result_array();
    }

    function jenis_jaminan()
    {
        $query = $this->db->query("select * from tm_jenis_jaminan");
        return $query->result_array();
    }

    function jenis_permohonan()
    {
        $query = $this->db->query("select * from tm_jenis_permohonan");
        return $query->result_array();
    }

    function pendidikan()
    {
        $query = $this->db->query("select * from tm_pendidikan");
        return $query->result_array();
    }

    function status_kawin()
    {
        $query = $this->db->query("select * from tm_status_kawin");
        return $query->result_array();
    }

    function status_rumah()
    {
        $query = $this->db->query("select * from tm_status_rumah");
        return $query->result_array();
    }

    function pengiriman_surat()
    {
        $query = $this->db->query("select * from tm_pengiriman_surat");
        return $query->result_array();
    }

    function jenis_tabungan()
    {
        $query = $this->db->query("select * from tm_jenis_tabungan");
        return $query->result_array();
    }

    function status()
    {
        $query = $this->db->query("select * from tm_status");
        return $query->result_array();
    }

    function status_tanah()
    {
        $query = $this->db->query("select * from tm_status_tanah");
        return $query->result_array();
    }

    function jangka_waktu()
    {
        $query = $this->db->query("select * from tm_jangka_waktu");
        return $query->result_array();
    }

    function agunan()
    {
        $query = $this->db->query("select * from tm_agunan");
        return $query->result_array();
    }

    function hubungan()
    {
        $query = $this->db->query("select * from tm_hubungan");
        return $query->result_array();
    }

    function penggarap()
    {
        $query = $this->db->query("select * from tm_penggarap");
        return $query->result_array();
    }

    function karakter()
    {
        $query = $this->db->query("select * from tm_karakter");
        return $query->result_array();
    }

    function kontruksi()
    {
        $query = $this->db->query("select * from tm_kontruksi");
        return $query->result_array();
    }

    function jenis_lantai()
    {
        $query = $this->db->query("select * from tm_jenis_lantai");
        return $query->result_array();
    }

    function atap()
    {
        $query = $this->db->query("select * from tm_atap");
        return $query->result_array();
    }

    function available()
    {
        $query = $this->db->query("select * from tm_available");
        return $query->result_array();
    }

    function jenis_kendaraan()
    {
        $query = $this->db->query("select * from tm_jenis_kendaraan");
        return $query->result_array();
    }

    function penggunaan_agunan()
    {
        $query = $this->db->query("select * from tm_penggunaan_agunan");
        return $query->result_array();
    }

     function daop_agunan()
    {
        $query = $this->db->query("select * from tm_daop_usaha");
        return $query->result_array();
    }

    function warna()
    {
        $query = $this->db->query("select * from tm_warna");
        return $query->result_array();
    }

    function approval()
    {
        $query = $this->db->query("select * from tm_approval where id_wil = ".$this->session->userdata('id_kantor')."");
        return $query->row();
    }

    function dokumen_beranda()
    {
        $query = $this->db->query("select COUNT(fak_id) as jumlahna from tt_fak where month(update_date) = month(current_date())");
        return $query->row();
    }

    function sumkredit_beranda()
    {
        $query = $this->db->query("select sum(jumlah_kredit_diajukan) as jumlahna from tt_fak where month(update_date) = month(current_date())");
        return $query->row();
    }

    function keterangan()
    {
        $query = $this->db->query("select * from tm_keterangan");
        return $query->result_array();
    }

}