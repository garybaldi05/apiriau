<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_m extends CI_Model
{
	function all_report()
	{
		$query = $this->db->query("select a.*, b.angsuran, d.nama_kantor, e.nama_kantor as perusahaan, g.nama_kantor as bank, c.app_status_7 
			from tt_fak a
			left join tt_mpk b on a.fak_id = b.fak_id 
			join tt_app c on a.fak_id = c.fak_id
			left join tt_akad f on a.fak_id = f.fak_id
			join tm_kantor d on a.id_kantor = d.id_kantor
			join tm_kantor e on a.id_perusahaan = e.id_kantor
			left join tm_kantor g on f.bank = g.id_kantor
			group by a.fak_id
			order by b.no_urut desc");
		return $query->result_array();
	}

	function bpr()
	{
		$query = $this->db->query("select * from tm_kantor where kantor_kategori = 1");
		return $query->result_array();
	}

	function perusahaan()
	{
		$query = $this->db->query("select * from tm_kantor where kantor_kategori = 4");
		return $query->result_array();
	}

	function bank_umum()
	{
		$query = $this->db->query("select * from tm_kantor where kantor_kategori = 2");
		return $query->result_array();
	}

	function status()
	{
		$query = $this->db->query("select * from tm_status_akad");
		return $query->result_array();
	}

	function filter($kantor,$dari,$sampai)
	{
		$query = $this->db->query("select a.*, b.angsuran, d.nama_kantor, e.nama_kantor as perusahaan, f.nama_kantor as bank, h.app_status_7 from tt_fak a
			left join tt_mpk b on a.fak_id = b.fak_id
			left join tt_app h on a.fak_id = h.fak_id
			left join tt_akad g on a.fak_id = g.fak_id
			join tm_kantor d on a.id_kantor = d.id_kantor
			join tm_kantor e on a.id_perusahaan = e.id_kantor
			left join tm_kantor f on g.bank = f.id_kantor
			where a.tanggal_permohonan between '$dari' and '$sampai' and
			a.id_kantor = $kantor
			group by a.fak_id
			order by b.no_urut desc");
		return $query->result_array();
	}

	function filter_perusahaan($kantor,$dari,$sampai)
	{
		$query = $this->db->query("select a.*, b.angsuran, d.nama_kantor, e.nama_kantor as perusahaan, f.nama_kantor as bank, h.app_status_7 from tt_fak a
			left join tt_mpk b on a.fak_id = b.fak_id
			left join tt_app h on a.fak_id = h.fak_id
			left join tt_akad g on a.fak_id = g.fak_id
			join tm_kantor d on a.id_kantor = d.id_kantor
			join tm_kantor e on a.id_perusahaan = e.id_kantor
			left join tm_kantor f on g.bank = f.id_kantor
			where a.tanggal_permohonan between '$dari' and '$sampai' and
			a.id_perusahaan = $kantor
			group by a.fak_id
			order by b.no_urut desc
			" );
		return $query->result_array();
	}

	function filter_bank($kantor,$dari,$sampai)
	{
		$query = $this->db->query("select a.*, b.angsuran, d.nama_kantor, e.nama_kantor as perusahaan, f.nama_kantor as bank, h.app_status_7 from tt_fak a
			left join tt_mpk b on a.fak_id = b.fak_id
			left join tt_akad c on a.fak_id = c.fak_id
			left join tt_app h on a.fak_id = h.fak_id
			join tm_kantor d on a.id_kantor = d.id_kantor
			join tm_kantor e on a.id_perusahaan = e.id_kantor
			left join tm_kantor f on c.bank = f.id_kantor
			where a.tanggal_permohonan between '$dari' and '$sampai' and
			c.bank = $kantor
			group by a.fak_id
			order by b.no_urut desc");
		return $query->result_array();
	}
}