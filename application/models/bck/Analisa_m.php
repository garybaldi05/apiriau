<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisa_m extends CI_Model
{

	// function __construct()
 //    {
 //        parent::__construct();
 //        $this->db2 = $this->load->database('database_kedua', TRUE);
 //        //databse ke 2
 //    }

	function fak_no()
	{
		$kodena	= $this->session->userdata('id_kantor');
		$this->db->select('RIGHT(tt_fak.fak_no,2) as no_order', FALSE);
		$this->db->order_by('fak_id','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tt_fak');      //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
		//jika kode ternyata sudah ada.      
		$data = $query->row();      
		$kode = intval($data->no_order) + 1;    
		}
		else {      
		//jika kode belum ada      
		$kode = 1;    
		}
		$kodemax = str_pad($kode++, 3, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		date_default_timezone_set('Asia/Jakarta');
		$kodejadi = "FAK0".$kodena.'-'.date('ym-'). $kodemax;    // hasilnya ODJ-9921-0001 dst.
		return $kodejadi;  
		
		// $tahun = date('y',strtotime($tglTran));
		// $bulan = date('m',strtotime($tglTran));
		// $tahun_bulan=$tahun.$bulan;
		// $kode_awal = $format_awal.'-'.$tahun_bulan.'-';
		
		// $no=$this->mod_global->f_GetDatatable("max(substring(".$field_kode.",".(strlen($kode_awal)+1).",".$nlength."))",$table_name,"where ".$field_kode." like '".$kode_awal."%'");
		// $no=$no+1;
		// if((int)$no >0){
		// 	if(strlen($no)==1){
		// 		$autono='00'.$no;
		// 	}elseif(strlen($no)==2){
		// 		$autono='0'.$no;
		// 	}elseif(strlen($no)==3){
		// 		$autono=$no;
		// 	}
		// }else{
		// 	$autono='001';
		// };
		
		// return $kode_awal.$autono;;
	}

	function apkm($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, b.*, c.app_keterangan_1 from tt_apkm a 
				left join tt_apkm_dokumen b on a.fak_id = b.fak_id 
				join tt_app c on a.fak_id = c.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function apkm_dokumen($fak_id)
	{
		$query = $this->db->query("select * from tt_apkm_dokumen where fak_id = $fak_id order by id_dokumen asc");
		return $query->result_array();
	}

	function dokumen($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a 
				left join tt_dokumen b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function list_analisa()
	{
		$query = $this->db->query("select a.fak_id, a.fak_no, a.nama_lengkap, a.jumlah_kredit_diajukan, a.tanggal_permohonan, a.no_ktp, e.kantor, a.update_by, d.nama, b.* from tt_fak a 
								join tt_app b on a.fak_id = b.fak_id
								left join tt_mpk c on a.fak_id = c.fak_id
								join tm_user d on a.update_by = d.nik
								join tm_kantor e on d.id_kantor = e.id_kantor
								where e.id_kantor = '".$this->session->userdata('id_kantor')."'");
		return $query->result_array();
	}

	function list_analisa_search($id_kantor,$dari,$sampai,$nama_lengkap)
	{
		$query = $this->db->query("select e.hasil_slik, a.fak_id, a.fak_no, a.nama_lengkap, a.no_ktp, a.create_date, b.nama, c.kantor, d.* 
						from tt_apkm a 
								left join tt_apkm_dokumen e on a.fak_id = e.fak_id
								join tm_user b on a.create_by = b.nik
								join tm_kantor c on b.id_kantor = c.id_kantor
								join tt_app d on a.fak_id = d.fak_id
								where c.id_kantor = $id_kantor and a.create_date between '".$dari."' and '".$sampai."' and a.nama_lengkap like '%".$nama_lengkap."%'
								group by a.fak_id");
		return $query->result_array();
	}

	function apkm_fak($fak_id)
	{
		$query = $this->db->query("select a.* from tt_fak a
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function lvd($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, b.* from tt_fak a left join tt_lvd b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function hpa_kios_1($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a 
				left join tt_hpa_kios_1 b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function hpa_takos_1($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a left join tt_hpa_takos_1 b on a.fak_id = b.fak_id where a.fak_id = $fak_id");
		return $query->row();
	}

	// function hpa_tb_1($fak_id)
	// {
	// 	$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.*, c.status as bertingkatna, d.kondisi as perawatanna, e.kontruksi, f.jenis_lantai, g.atap as atapna, h.available as kamar_mandina, i.available as wcna, j.available as pam_sumurna, k.available as listrikna, l.available as telepona, m.hubungan as hak_debitur, n.penggarap, o.status_tanah as status_tanahna, p.hubungan as hub_penghuni, q.status as cek_bpnna, r.status as hasilna from tt_fak a 
	// 			left join tt_hpa_tb_1 b on a.fak_id = b.fak_id
	// 			join tm_status c on b.bertingkat = c.id_status
	// 			join tm_kondisi d on b.perawatan = d.id_kondisi
	// 			join tm_kontruksi e on b.kontruksi_bangunan = e.id_kontruksi
	// 			join tm_jenis_lantai f on b.bentuk_lantai = f.id_jenis_lantai
	// 			join tm_atap g on b.atap = g.id_atap
	// 			join tm_available h on b.kamar_mandi = h.id_available
	// 			join tm_available i on b.wc = i.id_available
	// 			join tm_available j on b.pam_sumur = j.id_available
	// 			join tm_available k on b.listrik = k.id_available
	// 			join tm_available l on b.telepon = l.id_available
	// 			join tm_hubungan m on b.hubungan_hak_debitur = m.id_hubungan
	// 			join tm_penggarap n on b.status_penghuni = n.id_penggarap
	// 			join tm_status_tanah o on b.status_tanah = o.id_status_tanah
	// 			join tm_hubungan p on b.hubungan_penghuni = p.id_hubungan
	// 			join tm_status q on b.cek_bpn = q.id_status
	// 			join tm_status r on b.hasil_bpn = r.id_status
	// 			where a.fak_id = $fak_id");
	// 	return $query->row();
	// }

	function hpa_tb_1($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a 
				left join tt_hpa_tb_1 b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function hpa_tb_3($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a 
				left join tt_hpa_tb_3 b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	// function hpa_kendaraan_1($fak_id)
	// {
	// 	$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.*, c.jenis_kendaraan as jenis_kendaraanna, d.penggunaan_agunan as penggunaan_agunanna, e.daop_usaha, f.hubungan, g.status as cek_samsatna, h.warna as warnana, i.status as gesek_mesin, j.status as gesek_rangka, k.status as hasilna from tt_fak a  
	// 			left join tt_hpa_kendaraan_1 b on a.fak_id = b.fak_id
	// 			join tm_jenis_kendaraan c on b.jenis_kendaraan = c.id_jenis_kendaraan
	// 			join tm_penggunaan_agunan d on b.penggunaan_agunan = d.id_penggunaan_agunan
	// 			join tm_daop_usaha e on b.daop_agunan = e.id_daop_usaha
	// 			join tm_hubungan f on b.hubungan_pemilik_debitur = f.id_hubungan
	// 			join tm_status g on b.cek_samsat = g.id_status
	// 			join tm_warna h on b.warna = h.id_warna
	// 			join tm_status i on b.bukti_gesek_mesin = i.id_status
	// 			join tm_status j on b.bukti_gesek_rangka = j.id_status
	// 			join tm_status k on b.hasil = k.id_status
	// 			where a.fak_id = $fak_id");
	// 	return $query->row();
	// }

	function hpa_kendaraan_1($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a  
				left join tt_hpa_kendaraan_1 b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function hpa_kendaraan_3($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a  
				left join tt_hpa_kendaraan_3 b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function mpk($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, a.jumlah_kredit_diajukan, a.jangka_waktu as jangka_waktos, a.sisa_penghasilan, b.*, c.* from tt_fak a  
				left join tt_mpk b on a.fak_id = b.fak_id
				left join tt_app c on a.fak_id = c.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function mpkna_1($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 1 ");
		return $query->row_array();
	}

	function mpkna_2($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 2 ");
		return $query->row_array();
	}

	function mpkna_3($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 3 ");
		return $query->row_array();
	}

	function mpkna_4($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 4 ");
		return $query->row_array();
	}

	function mpkna_5($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 5 ");
		return $query->row_array();
	}

	function mpkna_6($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 6 ");
		return $query->row_array();
	}

	function mpkna_7($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 7 ");
		return $query->row_array();
	}

	function mpkna_8($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 8 ");
		return $query->row_array();
	}

	function mpkna_9($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 9 ");
		return $query->row_array();
	}

	function mpkna_10($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id and no_urut = 10 ");
		return $query->row_array();
	}

	function app_mpk($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a  
				left join tt_app b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id
				group by fakna");
		return $query->row();
	}

	function save_dok($fak_id,$image)
	{
		$data = array(
                'kelengkapan' => $image,
                'update_by'		=> $this->session->userdata('nik'),
                'update_date'		=> date('Y-m-d H:i:s')
            );  
        $result= $this->db->insert('tt_dokumen',$data);
        return $result;
	}

	function ladu($fak_id)
	{
		$query = $this->db->query("select a.fak_id as fakna, a.nama_lengkap, a.fak_no, b.* from tt_fak a  
				left join tt_ladu b on a.fak_id = b.fak_id
				where a.fak_id = $fak_id");
		return $query->row();
	}

	function dokumenna($fak_id)
	{
		$query = $this->db->query("select GROUP_CONCAT(ktp_url) as ktpna from tt_apkm_dokumen where fak_id = $fak_id");
		return $query->row_array();
	}

	function print_angsuran($fak_id)
	{
		$query = $this->db->query("select * from tt_mpk where fak_id = $fak_id order by no_urut desc");
		return $query->row();
	}

	function rekap()
	{
		$query = $this->db2->query("select a.kre_rekening, kre_bunga, a.kre_tgl_realisasi, a.kre_jkw, a.kre_tgl_jthtempo, a.kre_sistem_bunga, a.kre_plafon1, a.kre_baki_debet
			from data_kredit_master a 
			where a.kre_rekening like '%.330.%' and a.kre_sistem_bunga = 40 and a.kre_bunga = 24 and a.kre_baki_debet > 0");
		return $query->result_array();
	}

	function rank()
	{
		$query = $this->db->query("select SUM(a.jumlah_kredit_diajukan) as jumlahna, b.nama from tt_fak a 
			join tm_user b on a.update_by = b.nik
			where month(update_date) = month(current_date()) group by update_by order by jumlahna desc limit 4");
		return $query->result_array();
	}
	
}

			
