<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_m extends CI_Model
{
	function karyawan()
	{
		$query = $this->db->query("select * from tm_karyawan a join tm_jabatan b on a.id_jabatan = b.id_jabatan join tm_kantor c on a.id_kantor = c.id_kantor join tm_status d on a.id_status = d.id_status order by a.id_jabatan asc");
		return $query->result_array();
	}

	function karyawan_edit($id_karyawan)
	{
		$query = $this->db->query("select * from tm_karyawan a join tm_jabatan b on a.id_jabatan = b.id_jabatan join tm_kantor c on a.id_kantor = c.id_kantor where a.id_karyawan = $id_karyawan");
		return $query->row();
	}

	function jabatan()
	{
		$query = $this->db->query("select * from tm_jabatan");
		return $query->result_array();
	}

	function jabatan_edit($id_jabatan)
	{
		$query = $this->db->query("select * from tm_jabatan where id_jabatan = $id_jabatan");
		return $query->row();
	}

	function kantor()
	{
		$query = $this->db->query("select * from tm_kantor");
		return $query->result_array();
	}

	function kantor_edit($id_kantor)
	{
		$query = $this->db->query("select * from tm_kantor where id_kantor = $id_kantor");
		return $query->row();
	}

	function produk()
	{
		$query = $this->db->query("select * from tm_produk");
		return $query->result_array();
	}

	function produk_edit()
	{
		$query = $this->db->query("select * from tm_produk");
		return $query->row();
	}

	function status()
	{
		$query = $this->db->query("select * from tm_status ");
		return $query->result_array();
	}

	function user()
	{
		$query = $this->db->query("select * from tm_user a join tm_jabatan b on a.id_jabatan = b.id_jabatan join tm_kantor c on a.id_kantor = c.id_kantor order by a.id_jabatan asc");
		return $query->result_array();
	}

	function user_edit($id_user)
	{
		$query = $this->db->query("select * from tm_user where id_user = $id_user");
		return $query->row();
	}

	function jangka_waktu()
	{
		$query = $this->db->query("select * from tm_jangka_waktu");
		return $query->result_array();
	}

	function jenis_kelamin()
	{
		$query = $this->db->query("select * from tm_jenis_kelamin");
		return $query->result_array();
	}

	function pendidikan()
	{
		$query = $this->db->query("select * from tm_pendidikan");
		return $query->result_array();
	}

	function hubungan()
	{
		$query = $this->db->query("select * from tm_hubungan");
		return $query->result_array();
	}

	function approval()
	{
		$query = $this->db->query("select a.*, b.kantor from tm_approval a join tm_kantor b on a.id_wil = b.id_kantor");
		return $query->result_array();
	}

	
}