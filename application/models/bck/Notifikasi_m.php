<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi_m extends CI_Model
{
	function notif_ao()
	{
		$query = $this->db->query("select a.*, b.app_status_2, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 1 and a.status_baca = 0 and b.app_status_2 = 2 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_kresup()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 2 and a.status_baca = 0 and b.app_status_1 = 1 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_kaop()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 3 and a.status_baca = 0 and b.app_status_1 = 3 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_kabag()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 4 and a.status_baca = 0 and b.app_status_1 = 4 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_wapincab()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 5 and a.status_baca = 0 and b.app_status_1 = 2 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_pincab()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 6 and a.status_baca = 0 and b.app_status_1 = 2 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->result_array();
	}

	function notif_analis()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 7 and a.status_baca = 0 and b.app_status_1 = 2");
		return $query->result_array();
	}

	function notif_mb()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 8 and a.status_baca = 0 and b.app_status_1 = 2");
		return $query->result_array();
	}

	function notif_db()
	{
		$query = $this->db->query("select a.*, b.app_status_1, c.nama, c.id_kantor from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 9 and a.status_baca = 0 and b.app_status_1 = 2");
		return $query->result_array();
	}

	function count()
	{
		$query = $this->db->query("select count(id_pemberitahuan) as jumlah from tm_pemberitahuan a 
				join tt_app b on a.fak_id = b.fak_id
				join tm_user c on a.create_by = c.nik
				where c.id_jabatan = 2 and a.status_baca = 0 and b.app_status_1 = 2 and c.id_kantor = ".$this->session->userdata('id_kantor')."");
		return $query->row();
	}	
}