<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_m extends CI_Model
{

    function status_kawin()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'kawin' ");
        return $query->result_array();
    }

    function status_tempat()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'tempat' ");
        return $query->result_array();
    }

    function status_perkenalan()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'perkenalan' ");
        return $query->result_array();
    }

    function status_takeover()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'to' ");
        return $query->result_array();
    }

    function status_permohonan()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'permohonan' ");
        return $query->result_array();
    }

    function status_tempat_usaha()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'tempat_usaha' ");
        return $query->result_array();
    }

    function status_pekerjaan()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'pekerjaan' ");
        return $query->result_array();
    }

    function status_kondisi()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'kondisi' ");
        return $query->result_array();
    }

    function status_info_harga()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'info_harga' ");
        return $query->result_array();
    }

    function status_penggarap()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'penggarap' ");
        return $query->result_array();
    }

    function status_tanah()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'tanah' ");
        return $query->result_array();
    }

    function status_perawatan()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'kontruksi' ");
        return $query->result_array();
    }

    function status_kontruksi()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'kontruksi' ");
        return $query->result_array();
    }

    function status_lantai()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'lantai' ");
        return $query->result_array();
    }

    function status_atap()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'atap' ");
        return $query->result_array();
    }

    function status_ada_tidak()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'ada_tidak' ");
        return $query->result_array();
    }

    function status_rekomendasi()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'rekomendasi' ");
        return $query->result_array();
    }

    function status_debitur()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'debitur' ");
        return $query->result_array();
    }

    function status_nasabah2()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'nasabah2' ");
        return $query->result_array();
    }

    function status_lama_hubungan()
    {
        $query = $this->db->query("select * from tm_status where sts_jns = 'lama_hubungan' ");
        return $query->result_array();
    }


}