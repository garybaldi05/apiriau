<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from radixtouch.in/templates/admin/zivi/source/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 02:14:14 GMT -->

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?php echo $title; ?></title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url(); ?>assets/assets/img/logox.png' />

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">

  <!-- DATATABLE -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bundles/select2/dist/css/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bundles/bootstrap-daterangepicker/daterangepicker.css">
  

</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar sticky">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
									collapse-btn"> <i data-feather="align-justify"></i></a></li>
             <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
                <i data-feather="maximize"></i>
              </a></li>
            <li>
              <!-- <form class="form-inline mr-auto">
                <div class="search-element">
                  <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="200">
                  <button class="btn" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </form> -->
            </li>
          </ul>
        </div>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle">
            <!-- <a href="#" data-toggle="dropdown"
              class="nav-link nav-link-lg message-toggle"><i data-feather="mail"></i>
              <span class="badge headerBadge1">
                6 </span> </a> -->
            <!-- <div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
              <div class="dropdown-header">
                Messages
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-message">
                <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar
											text-white"> <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-1.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">John
                      Deo</span>
                    <span class="time messege-text">Please check your mail !!</span>
                    <span class="time">2 Min Ago</span>
                  </span>
                </a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
                    <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-2.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">Sarah
                      Smith</span> <span class="time messege-text">Request for leave
                      application</span>
                    <span class="time">5 Min Ago</span>
                  </span>
                </a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
                    <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-5.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">Jacob
                      Ryan</span> <span class="time messege-text">Your payment invoice is
                      generated.</span> <span class="time">12 Min Ago</span>
                  </span>
                </a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
                    <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-4.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">Lina
                      Smith</span> <span class="time messege-text">hii John, I have upload
                      doc
                      related to task.</span> <span class="time">30
                      Min Ago</span>
                  </span>
                </a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
                    <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-3.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">Jalpa
                      Joshi</span> <span class="time messege-text">Please do as specify.
                      Let me
                      know if you have any query.</span> <span class="time">1
                      Days Ago</span>
                  </span>
                </a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
                    <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/users/user-2.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">Sarah
                      Smith</span> <span class="time messege-text">Client Requirements</span>
                    <span class="time">2 Days Ago</span>
                  </span>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div> -->
          </li>

          <li class="dropdown dropdown-list-toggle">
            <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg message-toggle">
              <i data-feather="bell" class="bell"></i>
              <span id="jumlahna" class="badge headerBadge1"></span>
            </a>

            <div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
              <div class="dropdown-header">
                Pemberitahuan
              </div>

              <div class="dropdown-list-content dropdown-list-icons" id="list_notif">

              </div>

              <div class="dropdown-footer text-center">
                <a href="<?php echo base_url('notifikasi');?>">Lihat Semua <i class="fas fa-chevron-right"></i></a>
              </div>

            </div>
          </li>

          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/user.png" class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
            <div class="dropdown-menu dropdown-menu-right pullDown">
              <div class="dropdown-title">Hello <?php echo $this->session->userdata('nama'); ?></div>
              <a data-toggle="modal" data-target="#settings" class="dropdown-item has-icon"> <i class="fas fa-cog"></i>
                Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url('login/logout'); ?>" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i>
                Logout
              </a>
            </div>
          </li>

        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href=""> <span class="logo-name">AOS</span>
            </a>
          </div>
          <div class="sidebar-user">
            <div class="sidebar-user-picture">
              <img alt="image" src="<?php echo base_url(); ?>assets/assets/img/logoz.png" width="30%">
            </div>
            <div class="sidebar-user-details">
              <?php
                $user = $this->session->userdata('id_user');
                $q = $this->db->query("select a.*, b.nama_kantor, c.jabatan from tm_user a
                  join tm_kantor b on a.id_kantor = b.id_kantor
                  join tm_jabatan c on a.id_jabatan = c.id_jabatan
                 where a.id_user = $user")->row();
              ?>
              <div class="user-name"><?php echo $q->username .' - '. $q->jabatan; ?></div>
              <div class="user-role"><?php echo $q->nama_kantor; ?></div>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>

            <!-- <li>
              <a class="nav-link" href="<?php echo base_url('beranda'); ?>">
                <i data-feather="file"></i>
                <span>Beranda</span>
              </a>
            </li>

            <li class="dropdown active">
              <a href="#" class="menu-toggle nav-link has-dropdown">
                <i data-feather="monitor"></i><span>Master Data</span>
              </a>
              <ul class="dropdown-menu">
                <li class="active">
                  <a class="nav-link" href="<?php echo base_url('karyawan'); ?>">Karyawan</a>
                </li>
                <li>
                  <a class="nav-link" href="<?php echo base_url('user'); ?>">User</a>
                </li>
              </ul>
            </li>

            <li>
              <a class="nav-link" href="<?php echo base_url('analisa/index'); ?>">
                <i data-feather="file"></i>
                <span>Monitoring KMPS</span>
              </a>
            </li> -->

            <?php
            $main = $this->db->query("select distinct a.* from tm_menu a join tt_role_jabatan b on a.id_menu = b.id_menu where b.id_jabatan = '" . $this->session->userdata('id_jabatan') . "' and a.kat_menu = 0 and a.status = 1 order by a.id_menu asc");

            foreach ($main->result() as $m) {
              $sub = $this->db->get_where('tm_menu', array('kat_menu' => $m->id_menu));
              $sub = $this->db->query("select a.* from tm_menu a join tt_role_jabatan b on a.id_menu = b.id_menu where b.id_jabatan = '" . $this->session->userdata('id_jabatan') . "' and a.kat_menu = '" . $m->id_menu . "' and a.status = 1 order by a.id_menu asc");

              if ($sub->num_rows() > 0) {
                if ($sub_menu == $m->id_menu) {
                  $open = 'dropdown active';
                } else {
                  $open = '';
                }

                echo    '
                          <li class="'.$open.'">
                            <a href="' . base_url($m->link) . '" class="menu-toggle nav-link has-dropdown">
                              <i class="'.$m->icon.'"></i><span>' . $m->nama_menu . '</span>
                            </a>

                      ';
                echo "<ul class='dropdown-menu'>";

                foreach ($sub->result() as $s) {
                  if ($page_id == $s->id_menu) {
                    $aktif = 'active';
                  } else {
                    $aktif = '';
                  }
                  echo '
                        <li class="'.$aktif.'">
                          <a href="' . base_url($s->link) . '" class="nav-link">' . $s->nama_menu . '</a>
                        </li>
                ';
                }
                echo "</ul>";
                echo '</li>';
              } else {
                if ($page_id == $m->id_menu) {
                  $aktif = 'active';
                } else {
                  $aktif = '';
                }
                echo '<li>
                          <a class="nav-link" href="' . base_url($m->link) . '" >
                            <i class="' . $m->icon . '"></i>
                              <span>' . $m->nama_menu . '</span>
                          </a>
                        </li>

                        ';
              }
            }
            ?>

          </ul>




        </aside>
      </div>
      <!-- Main Content -->
      <div class="main-content">
        <?php echo $contents; ?>
        <div class="settingSidebar">
          <a href="javascript:void(0)" class="settingPanelToggle"> <i class="fa fa-spin fa-cog"></i>
          </a>
          <div class="settingSidebar-body ps-container ps-theme-default">
            <div class=" fade show active">
              <div class="setting-panel-header">Setting Panel
              </div>
              <div class="p-15 border-bottom">
                <h6 class="font-medium m-b-10">Select Layout</h6>
                <div class="selectgroup layout-color w-50">
                  <label class="selectgroup-item">
                    <input type="radio" name="value" value="1" class="selectgroup-input-radio select-layout" checked>
                    <span class="selectgroup-button">Light</span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="value" value="2" class="selectgroup-input-radio select-layout">
                    <span class="selectgroup-button">Dark</span>
                  </label>
                </div>
              </div>
              <div class="p-15 border-bottom">
                <h6 class="font-medium m-b-10">Sidebar Color</h6>
                <div class="selectgroup selectgroup-pills sidebar-color">
                  <label class="selectgroup-item">
                    <input type="radio" name="icon-input" value="1" class="selectgroup-input select-sidebar">
                    <span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Light Sidebar"><i class="fas fa-sun"></i></span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="icon-input" value="2" class="selectgroup-input select-sidebar" checked>
                    <span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Dark Sidebar"><i class="fas fa-moon"></i></span>
                  </label>
                </div>
              </div>
              <div class="p-15 border-bottom">
                <h6 class="font-medium m-b-10">Color Theme</h6>
                <div class="theme-setting-options">
                  <ul class="choose-theme list-unstyled mb-0">
                    <li title="white" class="active">
                      <div class="white"></div>
                    </li>
                    <li title="cyan">
                      <div class="cyan"></div>
                    </li>
                    <li title="black">
                      <div class="black"></div>
                    </li>
                    <li title="purple">
                      <div class="purple"></div>
                    </li>
                    <li title="orange">
                      <div class="orange"></div>
                    </li>
                    <li title="green">
                      <div class="green"></div>
                    </li>
                    <li title="red">
                      <div class="red"></div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="p-15 border-bottom">
                <div class="theme-setting-options">
                  <label class="m-b-0">
                    <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="mini_sidebar_setting">
                    <span class="custom-switch-indicator"></span>
                    <span class="control-label p-l-10">Mini Sidebar</span>
                  </label>
                </div>
              </div>
              <div class="p-15 border-bottom">
                <div class="theme-setting-options">
                  <label class="m-b-0">
                    <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="sticky_header_setting">
                    <span class="custom-switch-indicator"></span>
                    <span class="control-label p-l-10">Sticky Header</span>
                  </label>
                </div>
              </div>
              <div class="mt-4 mb-4 p-3 align-center rt-sidebar-last-ele">
                <a href="#" class="btn btn-icon icon-left btn-primary btn-restore-theme">
                  <i class="fas fa-undo"></i> Restore Default
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="formModal">Settings</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url(); ?>">

                <div class="form-group">
                  <label>Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i class="fas fa-file-archive"></i>
                      </div>
                    </div>
                    <input type="text" class="form-control" name="fak_no" value="<?php echo $this->session->userdata('username'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label>New Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i class="fas fa-file-archive"></i>
                      </div>
                    </div>
                    <input type="password" class="form-control" name="fak_no">
                  </div>
                </div>

                <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN</button>
                <button type="reset" class="btn btn-warning m-t-15 waves-effect">RESET</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; <?php echo date('Y'); ?> <div class="bullet"></div> Design By <a href="#">IT Wasa</a>
        </div>
        <div class="footer-right">
        </div>
      </footer>
    </div>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo base_url(); ?>assets/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo base_url(); ?>assets/assets/bundles/apexcharts/apexcharts.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/amcharts4/core.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/amcharts4/charts.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/amcharts4/animated.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/jquery.sparkline.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url(); ?>assets/assets/js/page/index.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo base_url(); ?>assets/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url(); ?>assets/assets/js/custom.js"></script>

  <!-- DATATABLE -->
  <script src="<?php echo base_url(); ?>assets/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url(); ?>assets/assets/js/page/datatables.js"></script>

  <script src="<?php echo base_url(); ?>assets/assets/bundles/sweetalert/sweetalert.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url(); ?>assets/assets/js/page/sweetalert.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/js/jquery.blockUI.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bundles/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/page/forms-advanced-forms.js"></script>
      <script src="<?php echo base_url(); ?>assets/assets/bundles/jquery-pwstrength/jquery.pwstrength.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/assets/js/custom.js"></script>
      <script src="<?php echo base_url(); ?>assets/assets/js/izal.js"></script>
      <script src="<?php echo base_url(); ?>assets/assets/js/page/create-post.js"></script>
      <!-- <script src="<?php echo base_url(); ?>assets/assets/bundles/cleave-js/dist/cleave.min.js"></script> -->

  <script type="text/javascript">
    $(document).ready(function(){
            $.get('<?php echo base_url('notifikasi/list');?>',function(data){
                $('#list_notif').html(data)
                e.preventDefault();
                $.ajax({
                    type : 'get',
                    url  : $(this).attr('href'),
                    success:function(){
                        loadData();
                    }
                });
            })
        });
</script>

<script type="text/javascript">
    $(document).ready(function(){
            $.get('<?php echo base_url('notifikasi/count');?>',function(data){
                $('#jumlahna').html(data)
                e.preventDefault();
                $.ajax({
                    type : 'get',
                    url  : $(this).attr('href'),
                    success:function(){
                        loadData();
                    }
                });
            })
        });
</script>
<script src="<?php echo base_url();?>assets/assets/js/simple.money.format.js"></script>
  <script type="text/javascript">
    $('.money').simpleMoneyFormat();
  </script>

</body>


<!-- Mirrored from radixtouch.in/templates/admin/zivi/source/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 02:16:29 GMT -->

</html>