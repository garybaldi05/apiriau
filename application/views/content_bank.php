      <!-- Main Content -->
<section class="section">
  <ul class="breadcrumb breadcrumb-style ">
    <li class="breadcrumb-item">
      <h4 class="page-title m-b-0">Dashboard</h4>
    </li>
    <li class="breadcrumb-item">
      <a href="index.html">
        <i class="fas fa-home"></i></a>
    </li>
    <li class="breadcrumb-item active">Dashboard</li>
  </ul>
  <div class="row ">
    <div class="col-xl-3 col-lg-6">
      <div class="card bg-cyan">
        <div class="card-statistic-3">
          <div class="card-content">
            <h4 class="card-title">New Orders</h4>
            <span>524</span>
            <div class="progress mt-1 mb-1" data-height="8">
              <div class="progress-bar l-bg-purple" role="progressbar" data-width="25%" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="mb-0 text-sm">
              <span class="mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
              <span class="text-nowrap">Since last month</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6">
      <div class="card bg-indigo">
        <div class="card-statistic-3">
          <div class="card-content">
            <h4 class="card-title">New Booking</h4>
            <span>1,258</span>
            <div class="progress mt-1 mb-1" data-height="8">
              <div class="progress-bar l-bg-orange" role="progressbar" data-width="25%" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="mb-0 text-sm">
              <span class="mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
              <span class="text-nowrap">Since last month</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6">
      <div class="card bg-green">
        <div class="card-statistic-3">
          <div class="card-content">
            <h4 class="card-title">Inquiry</h4>
            <span>10,225</span>
            <div class="progress mt-1 mb-1" data-height="8">
              <div class="progress-bar l-bg-red" role="progressbar" data-width="25%" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="mb-0 text-sm">
              <span class="mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
              <span class="text-nowrap">Since last month</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6">
      <div class="card bg-purple">
        <div class="card-statistic-3">
          <div class="card-content">
            <h4 class="card-title">Earning</h4>
            <span>$2,658</span>
            <div class="progress mt-1 mb-1" data-height="8">
              <div class="progress-bar l-bg-green" role="progressbar" data-width="25%" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="mb-0 text-sm">
              <span class="mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
              <span class="text-nowrap">Since last month</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-md-12 col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Chart</h4>
        </div>
        <div class="card-body">
          <div id="chart1"></div>
          <div class="statistic-details mt-1">
            <div class="statistic-details-item">
              <div class="text-small text-muted"><span class="text-primary"><i
                    class="fas fa-arrow-up col-green"></i></span> 12%</div>
              <div class="detail-value">$125</div>
              <div class="detail-name">Today</div>
            </div>
            <div class="statistic-details-item">
              <div class="text-small text-muted"><span class="text-danger"><i
                    class="fas fa-arrow-down col-red"></i></span> 33%</div>
              <div class="detail-value">$3,564</div>
              <div class="detail-name">This Week</div>
            </div>
            <div class="statistic-details-item">
              <div class="text-small text-muted"><span class="text-primary"><i
                    class="fas fa-arrow-up col-green"></i></span>19%</div>
              <div class="detail-value">$14,687</div>
              <div class="detail-name">This Month</div>
            </div>
            <div class="statistic-details-item">
              <div class="text-small text-muted"><span class="text-primary"><i
                    class="fas fa-arrow-down col-red"></i></i></span>29%</div>
              <div class="detail-value">$88,568</div>
              <div class="detail-name">This Year</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-12 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>TODO</h4>
        </div>
        <div class="card-body">
          <div class="tdl-content">
            <ul class="to-do-list ui-sortable" id="todo-scroll">
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Add salary details in system <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Announcement for holiday <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    call bus driver <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Office Picnic <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Website Must Be Finished <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Recharge My Mobile <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
              <li class="clearfix">
                <div class="form-check m-l-10">
                  <label class="form-check-label"> <input class="form-check-input" type="checkbox" value="">
                    Add salary details in system <span class="form-check-sign"> <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="todo-actionlist pull-right clearfix">
                  <a href="#"> <i class="material-icons">clear</i>
                  </a>
                </div>
              </li>
            </ul>
          </div>
          <div id="chat-form">
            <input type="text" class="form-control tdl-new" placeholder="Enter Todo...">
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-12 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>Order Status</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive" id="order-tbl-scroll">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Order No</th>
                  <th>Cust Name</th>
                  <th>Process</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tr>
                <td>XY56987</td>
                <td>John Deo</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="58%">
                    <div class="progress-bar bg-success" data-width="58"></div>
                  </div>
                </td>
                <td>$955</td>
              </tr>
              <tr>
                <td>XY12587</td>
                <td>Sarah Smith</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="85%">
                    <div class="progress-bar bg-purple" data-width="85"></div>
                  </div>
                </td>
                <td>$215</td>
              </tr>
              <tr>
                <td>XY58987</td>
                <td>Sarah Smith</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="35%">
                    <div class="progress-bar bg-orange" data-width="35"></div>
                  </div>
                </td>
                <td>$215</td>
              </tr>
              <tr>
                <td>XY56987</td>
                <td>John Deo</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="58%">
                    <div class="progress-bar bg-success" data-width="58"></div>
                  </div>
                </td>
                <td>$955</td>
              </tr>
              <tr>
                <td>XY12587</td>
                <td>Sarah Smith</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="85%">
                    <div class="progress-bar bg-purple" data-width="85"></div>
                  </div>
                </td>
                <td>$215</td>
              </tr>
              <tr>
                <td>XY58987</td>
                <td>Sarah Smith</td>
                <td class="align-middle">
                  <div class="progress" data-height="4" data-toggle="tooltip" title="35%">
                    <div class="progress-bar bg-orange" data-width="35"></div>
                  </div>
                </td>
                <td>$215</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
       