<section class="section">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    Home
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">
                    <i class="far fa-file text-warning"></i>
                    <?php echo $title; ?>
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">
                    <i class="fas fa-list text-success"></i>
                    Data
                </a>
            </li>
        </ol>
    </nav>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4><?php echo $title; ?></h4>
                    </div>

                    <div class="card-body">

                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">
                                    <i class="fas fa-user-friends"></i> Data
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">
                                    <i class="fas fa-user-plus text-warning"></i> Tambah
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content " id="myTab3Content">
                            <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Cabang</th>
                                                <th class="text-center">No PK</th>
                                                <th class="text-center">No Rekening</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">Tanggal Lahir</th>
                                                <th class="text-center">Tanggal Pembukaan</th>
                                                <th class="text-center">Jatuh Tempo</th>
                                                <th class="text-center">Plan Kredit</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">No KTP</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Jenis Kelamin</th>
                                                <th class="text-center">Rate Asuransi</th>
                                                <th class="text-center">Asuransi</th>
                                                <th class="text-center">No NPWP</th>
                                                <th class="text-center">No PK Lama</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php $no=1; foreach ($datana as $key => $value) { ?>
                            <tr id="<?php echo $value['id']; ?>">
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center"><?=$value['cab']?></td>
                                <td class="text-center"><?=$value['pk']?></td>
                                <td class="text-center"><?=$value['norek']?></td>
                                <td class="text-center"><?=$value['nama']?></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['lahir']));?></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['buka']));?></td>
                                <td class="text-center"><?=$value['tempo']?></td>
                                <td class="text-center"><?=$value['plankredit']?></td>
                                <td class="text-center"><?=number_format($value['amount'], 0);?></td>
                                <td class="text-center"><?=$value['ktp']?></td>
                                <td class="text-center"><?=$value['rate'];?></td>
                                <td class="text-center"><?php
                                    if ($value['sex'] == 'P'){
                                        ?>
                                    <span class="label label-info">
                                        <?php
                                        echo $value['sex'] = 'Perempuan';
                                        } else{
                                        ?>
                                        <span class="label label-warning">
                                        <?php
                                        echo $value['sex'] = 'Laki-Laki';
                                        }
                                        ?></td>
                                        <td class="text-center"><?=$value['rate_asuransi'];?></td>
                                <td class="text-center"><?=$value['asuransi']?></td>
                                <td class="text-center"><?=$value['npwp']?></td>
                                <td class="text-center"><?=$value['old_pk']?></td>
                            </tr>  
                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script src="<?php echo base_url();?>assets/assets/js/jquery-3.3.1.min.js"></script>
